var fs = require('fs');

var noOfUsers = 60;
var noOfRecipients = 8;
var noOfQuestions = 10;

var data = {

    "surveyInstance" : {

        "name" : "Quarter Four",
        "orgName" : "Engineering",
        "surveyName" : "Peer Feedback",
        "startDate" : "1463456734",
        "endDate" : "12463473453",

        "numberOfPeopleInOrg" : "60",

        "numberOfProviderUsers" : "59",
        "numberOfRecipientUsers" : "58",

        "numberOfSurveyCompletions" : "472",

        "users": [],
        "questions": [],
        "answers" : []
    }
}

// setup questions

var questions = [
    {'text':'Do they have the healing factor like Wolverine?', 'type':'range'},
    {'text':'Do they beat Wonder woman and be an omni linguist?', 'type':'range'},
    {'text':'Do they read your mind and uproot trees and cards like Jean Gray?', 'type':'range'},
    {'text':'Would they have a two communication channel to your mind like Professor X?', 'type':'range'},
    {'text':'Do they sping webs and act cool like Spider Man? And then dont get included in The Avengers?', 'type':'range'},
    {'text':'Would they manipuate ice and skate and their heart pleases like Iceman?', 'type':'range'},
    {'text':'How often do they fly like a bird or like a plane or like Superman?', 'type':'range'},
    {'text':'Do they disappear in Engineering and appear in the Kitchen like Nightcrawler?', 'type':'range'},
    {'text':'Do they stretch from their seats and pair programming with the other guy down the aisle like Mr. Fantastic?', 'type':'range'},
    {'text':'Do they turn green and break monitors when they have been working far too long with a bug like Hulk?', 'type':'range'},
    {'text':'What are their other superpowers?', 'type':'open'},
    {'text':'Do they have an achilles heel?', 'type':'open'}
];

questions.forEach(function(currentValue, index, array) {

    var q = {
        "text": currentValue.text,
        "type": currentValue.type,
        "id": index,
        "rangeStart": 0,
        "rangeEnd": 5
    };

    data.surveyInstance.questions.push(q);
});

// for each user

for (var user = 0; user < noOfUsers; user++) {

    var providerId = user + 1;
    var providerName = ['John Doe', 'Jane Doe'][Math.floor(Math.random() * 200)%2] + ' ' + String.fromCharCode(65 + user%26) + user%26;
    var providerEmail = providerName.toLowerCase().replace(/\s/,'.') + '@ensighten.com';
    var providerType = ['Engineer', 'Product Manager', 'Architect'][Math.floor(Math.random() * (2 - 0) + 0)];
    var providerManagerName = ['John Moe', 'Jane Moe', 'Joe Moe'][Math.floor(Math.random() * (2 - 0) + 0)]

    var u = {

        "id": providerId,
        "name": providerName,
        "email": providerEmail,
        "type": providerType,
        "managerName": providerManagerName
    };

    data.surveyInstance.users.push(u);

    // for each of the 8 survey she gives

    for (var recipient = user - 4; recipient < user - 4 + noOfRecipients; recipient++) {

        // for each of the 10 questions in the survey

        var recipientId = recipient + 1;
        var recipientName = ['John Doe', 'Jane Doe'][Math.floor(Math.random() * 200)%2] + ' ' + String.fromCharCode(65 + user%26) + user%26;
        var recipientEmail = recipientName.toLowerCase().replace(/\s/,'.') + '@ensighten.com';
        var recipientType = ['Engineer', 'Product Manager', 'Architect'][Math.floor(Math.random() * (2 - 0) + 0)];
        var recipientManagerName = ['John Moe', 'Jane Moe', 'Joe Moe'][Math.floor(Math.random() * (2 - 0) + 0)]

        for (var question = 0; question < noOfQuestions; question++) {

            var qText = questions[question%10].text;
            var qTypeName = questions[question%10].type;

            var answers = ['What?', 'ForgetIt', 'Please.', 'Yo', 'TheyreKilling'];

            var r = Math.floor(Math.random() * (5 - 0));

            var answerOptionText = answers[r];
            var answerOptionValue = r+1;

            var answer = {

                "pId" : providerId,
                "pN" : providerName,
                "pE" : providerEmail,
                "pT" : providerType,
                "pM" : providerManagerName,

                "rId" : recipientId,
                "rN" : recipientName,
                "rE" : recipientEmail,
                "rT" : recipientType,
                "rM" : recipientManagerName,

                "qId": question,
                "qT" : qText,
                "qTy" : qTypeName,
                "qR" : "0-5",

                "aT" : answerOptionText,
                "aV" : answerOptionValue
            }

            data.surveyInstance.answers.push(answer);
        }
    }
}

fs.writeFile('../si/id/data.json', JSON.stringify(data, null, 2), function(err) {

    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
