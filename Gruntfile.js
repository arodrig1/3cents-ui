module.exports = function(grunt) {

    // load all grunt tasks form dependencies, devDependencies, and peerDependencies.
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        clean: ['public/css/compiled'],

        // less compilation
        // ------------------------------------------------------------

        less: {

            dev: {
                files: {
                    "public/css/compiled/main.css": "public/css/src/main.less"
                },
                options: {
                    sourceMap: true,
                    sourceMapFilename: 'public/css/compiled/main.css.map',
                    sourceMapURL: '/css/compiled/main.css.map',
                    sourceMapBasePath: 'public',
                    sourceMapRootPath: '/'
                }
            },

            build: {
                files: {
                    "public/css/compiled/main.css": "public/css/src/main.less"
                },
                options: {
                    cleancss: true
                }
            }
        },

        // karma configuration
        // ------------------------------------------------------------

        karma: {

            continous: {
                configFile: 'tests/unit/config.js',
                browsers: ['PhantomJS'],
                singleRun: false
            },

            once: {
                configFile: 'tests/unit/config.js',
                browsers: ['Chrome'],
                singleRun: true
            }
        },

        protractor_webdriver: {

            options: {
                command: 'webdriver-manager start',
            },

            your_target: {
              // Target-specific file lists and/or options go here.
            },
          },

        protractor: {

            once: {

                configFile: "tests/end-to-end/config.js",
                keepAlive: false,
            }
        },

        concurrent: {

            dev: {

                tasks: ['nodemon:dev', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        express: {

            options: {
              // Override defaults here
            },

            test: {
              options: {
                script: 'app.js',
                node_env: 'development'
              }
            }
        },


        nodemon: {

            dev: {
                script: 'app.js',
                options: {

                    env: {

                        NODE_ENV: 'development'
                    },

                    callback: function (nodemon) {

                        nodemon.on('log', function (event) {
                            console.log(event.colour);
                        });

                        // opens browser on initial server start
                        nodemon.on('config:update', function () {
                            // Delay before server listens on port
                            setTimeout(function() {

                                // UI
                                require('open')('http://localhost:12002');

                                // Redis commander
                                // require('open')('http://localhost:8081');

                            }, 1000);
                        });

                        // refreshes browser when server reboots
                        nodemon.on('restart', function () {
                            // Delay before server listens on port
                            setTimeout(function() {
                                require('touch')('.reboot');
                            }, 1000);
                        });
                    }
                }
            }
        },

        // Start redis server and add in session
        external_daemon: {
            redis: {
                cmd: 'redis-server'
            },
            redisSetApiKey: {
                cmd: 'redis-cli',
                args: [
                    'set',
                    'devsid',
                    'devApiKey'
                ]
            },
            redisSetSession: {
                cmd: 'redis-cli',
                args: [
                    'hmset',
                    'devApiKey',
                    'username',
                    'development',
                    'account',
                    'Saravanan Ganesh',
                    'rolePermissions',
                    'view activate,manage activate,create clientside condition,create space,edit space,delete space,create condition,delete condition,edit condition,view role,manage role,view user,manage user,view data layer,manage data layer,view mobile,view report,create deployment,edit deployment,delete deployment,deploy deployment,view space,merge space,publish space,product inform,product mobile optimization,product reporting'
                ]
            }
        },

        // Start local java api
        shell: {
            javaApiBuild: {
                command: ['cd $JAVA_API_PATH', './gradlew clean bootRepackage -x \'test\''].join('&&')
            },
            javaApiLaunch: {
                command: 'osascript ./grunt_shell_scripts/LaunchApi.applescript'
            }
        },


        // WATCHER - LIVE RELOADS ON CSS, JS, HTML, NODE CHANGES
        watch: {
            options: {
                livereload: true
            },
            css: {
                files: 'public/css/src/**/*.{less,css}',
                tasks: ['less:dev'],
                options: {
                    debounceDelay: 1000
                }
            },
            js : {
                files: 'public/scripts/app/*.js',
                options: {
                    debounceDelay: 1000
                }
            },
            html: {
                files: 'public/templates/**/*.html',
                options: {
                    debounceDelay: 1000
                }
            },
            json: {
                files: 'public/api/**/*.json',
                options: {
                    debounceDelay: 1000
                }
            },
            reboot: {
                files: '.reboot'
            }
        }
    });

    // develop task with the api - until you set it up :P
    grunt.registerTask('dev', ['clean', 'less:dev', 'external_daemon:redis', 'external_daemon:redisSetApiKey', 'external_daemon:redisSetSession', 'concurrent:dev']);

    // test task ( used before publishing to stage / prod )
    grunt.registerTask('test', ['clean','less:build', 'external_daemon:redis', 'karma:once', 'express:test', 'protractor_webdriver', 'protractor']);

    // default task : develop
    grunt.registerTask('default', ['dev']);
};
