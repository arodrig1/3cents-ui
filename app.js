/**
*
* CENTS UI SERVER
* @author : srrvnn
* @code-reuse : Ensighten Mobile UI
*
*/

var express = require('express'),
    bodyParser = require('body-parser');
    cookieParser = require('cookie-parser'),
    logger = require('morgan');
    fs = require('fs'),
    http = require('http'),
    httpProxy = require('http-proxy'),
    NODE_ENV = process.env.NODE_ENV;

// set default configuration values

var config = {

    "api":{

        "host": "localhost",
        "port": 8080
    },
    "redis": {

        "host": "localhost",
        "port": 6379
    }
};

var app = express();
var port = 12002;

// create UI HTTP server

var server = http.createServer(app).listen(port, function() {
    console.log("Express server now running on " + port);
});

// create Redis client to check manage sessions

var redis = require('redis').createClient(config.redis.port, config.redis.host);

// create API proxy and middle layer function

var apiProxy = httpProxy.createProxyServer({

    target: 'http://' + config.api.host + ":" + config.api.port
});

var apiCallParser = function() {
    return function(req, res, next) {
        if (req.url.match(/\/api\/.*/)) { // CHANGE API ROUTING PATTERN HERE
            return apiProxy.proxyRequest(req, res, {
                host: config.api.host,
                port: config.api.port
            });
        } else {
            return next();
        }
    };
};

app.use(logger('combined'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(checkSession);
app.use(express.static(__dirname + '/public'));
app.use(apiCallParser());

function checkSession(req, res, next) {

    console.log('checking session');

    checkAndSetHeaders(req, res, function(result) {

        if (result.id) {

            return next(); // authenticated, proceed

        } else {

            res.redirect('/'); // not authenticated, redirect to manage
        }
    });
}

function checkManageSession(req, callback) {

    // return dummy info for development

    var manageSid = req.cookies['manage.sid'];

    if (process.env.NODE_ENV === 'development') {

        manageSid = 'devsid'
        // req.apiKey = 'development';
        // req.account = 'mobile_training';

        // return callback(null, {
        //     id: 'development',
        //     sessionData: {

        //         username: 'development',
        //         account: 'mobile_training',
        //         rolePermissions: 'view activate,manage activate,create clientside condition,create space,edit space,delete space,create condition,delete condition,edit condition,view role,manage role,view user,manage user,view data layer,manage data layer,view mobile,view report,create deployment,edit deployment,delete deployment,deploy deployment,view space,merge space,publish space,product inform,product mobile optimization,product reporting'
        //     }
        // });
    }

    redis.get(manageSid, function(err, apiKey) {

        if (apiKey) {

            req.apiKey = apiKey;
            redis.hgetall(apiKey, function(err, apiSessionData) {

                // TODO : Handle error case for null accounts

                if ( apiSessionData && apiSessionData.account ) {
                    req.account = apiSessionData.account.toLowerCase();
                } else {
                    console.log("Redis returned a null session!");
                    return callback(null, {});
                }

                return callback(null, {id: apiKey, sessionData: apiSessionData});
            });

        } else {
            console.log("No valid session key!");
            return callback(null, {});
        }

    });
}

function checkAndSetHeaders(req, res, callback){
    checkManageSession(req, function(err, result) {
        if (result.id) {

            res.cookie('apiKey', result.id, { expires: new Date(Date.now() + (1800*1000)), path : '/' });
            res.cookie('username', result.sessionData.username, { expires: new Date(Date.now() + (1800*1000)), path : '/' });
            res.cookie('account', result.sessionData.account.toLowerCase(), { expires: new Date(Date.now() + (1800*1000)), path : '/' });
            res.cookie('rolePermissions', result.sessionData.rolePermissions, { expires: new Date(Date.now() + (1800*1000)), path : '/' });

            // res.setHeaders('Set-Cookie', 'rolePermissions='+result.sessionData.rolePermissions+'; expires='+new Date(new Date().getTime()+(1800*1000)).toUTCString()+'; path=/');
            // res.setHeaders('Set-Cookie', 'apiKey='+result.id+'; expires='+new Date(new Date().getTime()+(1800*1000)).toUTCString()+'; path=/');
            // res.setHeaders('Set-Cookie', 'username='+result.sessionData.username+'; expires='+new Date(new Date().getTime()+(1800*1000)).toUTCString()+'; path=/');
            // res.setHeaders('Set-Cookie', 'account='+result.sessionData.account.toLowerCase()+'; expires='+new Date(new Date().getTime()+(1800*1000)).toUTCString()+'; path=/');

        } else {
            res.setHeader('Set-Cookie', 'apiKey=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/');
            res.setHeader('Set-Cookie', 'username=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/');
            res.setHeader('Set-Cookie', 'account=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/');
            res.setHeader('Set-Cookie', 'rolePermissions=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/');
        }
        callback(result);
    });
}

app.get('/validate', function(req,res) {
    checkAndSetHeaders(req, res, function(result) {
        if (result.id){
            res.redirect('/');
        } else {
            res.redirect('/');
        }
    });
});

app.get('/', function(req,res) {
    checkAndSetHeaders(req, res, function(result) {
        if (result.id){
            res.redirect('/index.html');
        } else {
            res.redirect('/');
        }
    });
});

app.get('/sanity', function(req, res) {
    res.send("SUCCESS");
});

process.on('uncaughtException', function (err) {

    // Log error to error.log
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    console.error("Status Code : " + err.statusCode);
    console.error(err);
    process.exit(1);

});
