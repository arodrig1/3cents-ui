requirejs.config({

    baseUrl: '/lib',
    paths: {

        'angucomplete-alt': 'angucomplete-alt/dist/angucomplete-alt.min',
        'angular': 'angular/angular.min',
        'angularCookies': 'angular-cookies/angular-cookies.min',
        'angularRoute': 'angular-route/angular-route.min',
        'app': '../js/app',
        'd3': 'd3/d3.min',
        'jquery': 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min',
        'moment' : 'moment/min/moment.min'
    },

    shim: {

        'angucomplete-alt' : ['angular'],
        'angular': { deps: ['jquery'], exports: 'angular'},
        'angularCookies': ['angular'],
        'angularRoute': ['angular']
    },
    priority: [
        'angular'
    ]
});

window.name = "NG_DEFER_BOOTSTRAP";

requirejs([

    'angular',
    'app/main',
    'app/routes'

], function(angular, app, routes) {

    'use strict';
    var $html = angular.element(document.getElementsByTagName('html')[0]);
    angular.element().ready(function() {
        $html.addClass('ng-app');
        angular.bootstrap($html, [app['name']]);
    })
})
