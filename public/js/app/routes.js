define([

    'angular',
    'app/main'

], function(angular, app) {

    'use strict';

    return app.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/login', {
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        });

        $routeProvider.when('/dashboard', {
            templateUrl: 'templates/dashboard.html',
            controller: 'DashboardCtrl'
        });

        $routeProvider.when('/survey/:surveyId/:recipientId', {
            templateUrl: 'templates/survey.html',
            controller: 'SurveyCtrl'
        });

        $routeProvider.when('/admin', {
            templateUrl: 'templates/admin.html',
            controller: 'AdminCtrl'
        });

        $routeProvider.when('/admin/:surveyInstanceId', {
            templateUrl: 'templates/admin.html',
            controller: 'AdminCtrl'
        });

        $routeProvider.when('/d3', {
            templateUrl: 'templates/d3.html',
            controller: 'D3Ctrl'
        });

        $routeProvider.otherwise({
            redirectTo: '/dashboard'
        });
    }])

});
