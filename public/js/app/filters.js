define([

    'angular'
    
], function (angular) {
    
    return angular.module('mobileUI.filters', [])

        .filter('secondsToHMS', function() {
            return function(seconds) {

                if (!seconds) {
                    return '';
                }
                if (typeof seconds !== 'number') {
                    return '';
                }
                if (seconds < 0) {
                    return '';
                }

                var secondsInAMinute = 60;
                var secondsInAnHour = secondsInAMinute * 60;

                var hours = Math.floor(seconds / secondsInAnHour);
                var minutes = Math.floor((seconds % secondsInAnHour) / secondsInAMinute);
                var seconds = Math.floor(((seconds % secondsInAnHour) % secondsInAMinute));

                var hms = '';

                if (hours > 0) {
                    hms += hours + ':';
                    if (minutes < 10) {
                        hms += '0';
                    }
                }
                hms += minutes + ':';
                if (seconds < 10) {
                    hms += '0' + seconds;
                }
                else {
                    hms += seconds;
                }

                return hms;
            };
        })
});
