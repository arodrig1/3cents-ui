define([

    'angular',
    'app/services',
    'd3'

], function (angular, services, d3) {

    return angular.module('mobileUI.controllers', ['mobileUI.services', 'angucomplete-alt'])

        .controller('DashboardCtrl', function ($scope, $location, AuthService, DashboardService, AdminService) {

            $scope.type = 'Dashboard';

            $scope.showMyResponsesGraph = false;
            $scope.skewed = false;

            // get all info for dashboard and update

            DashboardService.getNextDeadline()
                .then(function(deadline) {

                    $scope.deadlineDueIn = deadline.dueIn,
                    $scope.deadlineDate = deadline.date

                    return DashboardService.getNextSurveyInstance();

                }).then(function(surveyInstance) {

                    $scope.surveyInstance = surveyInstance;

                    return DashboardService.getNextSurveyInstanceRecipients();

                }).then(function(surveyInstanceRecipients) {

                    $scope.surveyInstanceRecipients = surveyInstanceRecipients;

                    return DashboardService.getNextSurveyInstanceUsers();

                }).then(function(surveyInstanceUsers) {

                    $scope.surveyInstanceUsers = surveyInstanceUsers;

                    return AdminService.getNextSurveyInstanceData()

                }).then(function(surveyResponses) {

                    // filter my responses // TODO : implement a new api end point

                    var myResponses = surveyResponses.answers.filter(function(obj) {

                        if (obj.pId == AuthService.getUserId()) return true;
                        else false;
                    })

                    // calculate the distribution

                    var dist = {};

                    for (var myResponsesIndex = 0; myResponsesIndex < myResponses.length; myResponsesIndex++) {

                        var response = myResponses[myResponsesIndex];

                        dist[response.aV] = dist[response.aV] || 0;
                        dist[response.aV] = parseInt(dist[response.aV]) + 1;
                    }

                    $scope.myResponsesDist = dist;

                    buildBellCurve();
                    checkDataForSkews();
                })

            // functions to add recipients

            $scope.selectNewRecipient = function(r) {

                if (!r.originalObject) return;

                $scope.newRecipient = r.originalObject;
            };

            $scope.requestNewRecipient = function() {

                if (!$scope.newRecipient) return;

                DashboardService.newRecipientForSurveyInstance($scope.newRecipient.id)
                    .then(function() {

                        $scope.newRecipient = null;
                        return DashboardService.getNextSurveyInstanceRecipients();

                    }).then(function(surveyInstanceRecipients) {

                        $scope.surveyInstanceRecipients = surveyInstanceRecipients;

                    });
            };

            // functions to add providers

            $scope.selectNewProvider = function(r) {

                if (!r.originalObject) return;

                $scope.newProvider = r.originalObject;
            };

            $scope.requestNewProvider = function() {

                if (!$scope.newProvider) return;

                // console.log('will send request to add ' + $scope.newRecipient.name);

                DashboardService.newProviderForSurveyInstanceWithId($scope.newProvider.id)
                    .then(function() {

                        // show okay message

                        // clear selection from auto complete alt

                        $scope.newProvider = null;
                    });
            };

            // function to calculate and show skewed response warnings

            $scope.toggleMyResponsesGraph = function() {

                $scope.showMyResponsesGraph = !$scope.showMyResponsesGraph;
            }

            function buildBellCurve() {

                var data = [];

                for (var d in $scope.myResponsesDist) {

                    if (!$scope.myResponsesDist.hasOwnProperty(d)) return;

                    data.push({

                        p : $scope.myResponsesDist[d],
                        q : parseInt(d)
                    });
                }

                // getData(); // popuate data

                // line chart based on http://bl.ocks.org/mbostock/3883245
                var margin = {
                        top: 20,
                        right: 20,
                        bottom: 30,
                        left: 50
                    },
                    width = 902 - margin.left - margin.right,
                    height = 500 - margin.top - margin.bottom;

                var x = d3.scale.linear()
                    .range([0, width]);

                var y = d3.scale.linear()
                    .range([height, 0]);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .ticks(5)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left");

                var line = d3.svg.line()
                    .x(function(d) {
                        return x(d.q);
                    })
                    .y(function(d) {
                        return y(d.p);
                    });

                var svg = d3.select("#s-warning-d3").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                x.domain(d3.extent(data, function(d) {
                    return d.q;
                }));
                y.domain(d3.extent(data, function(d) {
                    return d.p;
                }));

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis);

                svg.append("path")
                    .datum(data)
                    .attr("class", "line")
                    .attr("d", line);
            }

            function checkDataForSkews() {

                var d = $scope.myResponsesDist;

                // assuming it is data with five values

                if (d[3] < d[1] || d[3] < d[2]  || d[3] < d[4]  || d[3] < d[5]) $scope.skewed = true;
                else $scope.skewed = false;
            }
        })

        .controller('SurveyCtrl', function ($scope, $location, $routeParams, SurveyService, UserService, AuthService) {

            $scope.selected = '';

            $scope.surveyInstance = {};
            $scope.survey = {};
            $scope.recipient = {};

            $scope.progress = '0%';
            $scope.current = 0;

            UserService.getUserById($routeParams['recipientId'])
                .then(function(data) {

                    $scope.recipient = data;
                })

            var surveyInstanceId = $routeParams['surveyId'];

            SurveyService.getInstanceById(surveyInstanceId)
                .then(function(data){

                    $scope.surveyInstance = data;
                    return SurveyService.getSurveyById(surveyInstanceId);

                }).then(function(data) {

                    $scope.survey = data;
                    var questions = $scope.survey.questions;

                    for (var q  = 0; q < questions.length; q++) {

                        var question = questions[q];
                        question.answer = {

                            optionId : '',
                            optionText : ''
                        };
                    }

                    console.log($scope.survey.questions);

                    $scope.unitProgress = (100 / $scope.survey.questions.length);

                    $scope.progress = $scope.unitProgress + '%';
                });

            function updateProgress(increase) {

                var percent = parseInt($scope.progress);

                if (increase) {

                    percent = percent + $scope.unitProgress;

                } else percent = percent - $scope.unitProgress;

                $scope.progress = Math.min(Math.max(percent, $scope.unitProgress), 100) + '%';
            }

            $scope.next = function() {

                $scope.current = Math.min($scope.current + 1, $scope.survey.questions.length-1);
                var id = $scope.survey.questions[$scope.current].questionId;

                document.getElementById(id).scrollIntoView();
                updateProgress(true);
            }

            $scope.previous = function() {

                $scope.current = Math.max($scope.current - 1, 0);
                updateProgress(false);
            }

            $scope.save = function() {

                var data = {

                    surveyInstanceId : '1', // get from url
                    recipientId : '2', // get from url
                    complete : 'true',
                    userId : AuthService.getUserId(),
                    answers : []
                };

                for (var q = 0; q < $scope.survey.questions.length; q++) {

                    var question = $scope.survey.questions[q];

                    data.answers.push({

                        userId : AuthService.getUserId(),
                        recipientId: '2', // todo get from url
                        instanceId: '1', // todo get from url
                        qId: question.questionId,
                        optionId: question.answer.optionId,
                        answerText: question.answer.optionText
                    });

                    if (!question.answer.optionId && !question.answer.optionText) data.complete = 'false';
                }

                // console.log('see if the answer is here');
                // console.dir($scope.survey.questions);

                console.log('survey completion data');
                console.dir(data);

                SurveyService.saveAnswersForSurveyInstance(data)
                    .then(function() {


                    });
            }
        })

        .controller('AdminCtrl', function ($scope, $location, AuthService, AdminService) {

            $scope.type = 'Survey';
            $scope.surveyInstanceId = '1';
            $scope.data = null;
            $scope.users = [];
            $scope.questions = [];

            $scope.filters = ['Graph Type', 'Graph Scope', 'Graph Question'];

            $scope.filterOptions = {

                'Graph Type' : ['Aster Plot', 'Distribution'],
                'Graph Scope' : ['All Together', 'All Seperately', 'Individual'],
            }

            $scope.selected = {

                'Graph Type' : '',
                'Graph Scope' : '',
                'Graph User': '',
                'Graph Question' : '',
            }

            $scope.select = function(type, value) {

                if (type == 'Graph Specificity') value = $scope.questionsReverseHash(value);
                if (value) $scope.selected[type] = value.trim();

                console.dir($scope.selected);

                drawGraphs();
            }

            function drawGraphs() {

                console.log($scope.selected);
            }

            AdminService.getNextSurveyInstanceData($scope.surveyInstanceId)
                .then(function(data) {

                    $scope.data = data.surveyInstance;
                    $scope.usersHash = {};
                    $scope.questionsHash = {};
                    $scope.questionsReverseHash = {};

                    // calculate unique users and unique questions from data;

                    for (var answer = 0; answer < $scope.data.answers.length; answer++) {

                        var thisAnswer = $scope.data.answers[answer];

                        $scope.usersHash[thisAnswer.rId] = {'userId': thisAnswer.rId, 'userName' : thisAnswer.rN};
                        $scope.questionsHash[thisAnswer.qId] = {'questionId': thisAnswer.qId, 'questionText' : thisAnswer.qT};
                        // $scope.questionsReverseHash[thisAnswer.qT] = thisAnswer.qId;
                    }

                    // console.log($scope.usersHash);

                    for (u in $scope.usersHash) {

                        var obj = $scope.usersHash[u];

                        $scope.users.push(obj);
                    }

                    // console.log($scope.users);

                    // // console.log('There are ' + $scope.users.length + ' users with responses for them');

                    for (q in $scope.questionsHash) {

                        var obj = $scope.questionsHash[u];

                        $scope.questions.push(obj);
                    }

                    // console.log($scope.questions);

                    // console.log('There are ' + $scope.questions.length + ' questions with responses');

                    // $scope.filterOptions.users = $scope.users;
                    // $scope.filterOptions['Graph Question'] = $scope.questions;

                });
        })

        .controller('D3Ctrl', function ($scope, $location, AuthService, DashboardService, AdminService) {
            $scope.graphTypes = [
                {
                    id: 0,
                    name: "Aster Plot"
                },
                {
                    id: 1,
                    name: "Distribution"
                },
                {
                    id: 2,
                    name: "Network"
                },
            ];

            $scope.selectedGraphType = $scope.graphTypes[0];
            $scope.selectGraphType = function (graphType) {
                $scope.selectedGraphType = graphType;
            };

            $scope.selectedUser = null;
            $scope.selectUser = function (user) {
                $scope.selectedUser = user;
            };

            $scope.selectedQuestion = null;
            $scope.selectQuestion = function (question) {
                $scope.selectedQuestion = question;
            };

            $scope.type = 'Admin';
            $scope.dd = {}; // that's our data

            AdminService.getNextSurveyInstanceData()
                .then(function (data) {

                    $scope.dd = data;

                    console.log('data from adminservice : ');
                    console.dir(data);

                    $scope.users = data.users;
                    $scope.questions = data.questions;
                    $scope.answers = data.answers;

                });

            $scope.appendGraph = function (graphType, user, question) {
                angular.element("svg").remove();
                angular.element("#d3_report").empty();

                switch (graphType.id) {
                    case 0: {
                        appendAsterPlot(user);
                        break;
                    }
                    case 1: {
                        appendDistributionPlot(user, question);
                        break;
                    }
                    case 2: {
                        appendNetworkGraph(user, question);
                        break;
                    }
                }
            };

            var appendAsterPlot = function (user) {
                var colors = [
                    "#9E0041",
                    "#C32F4B",
                    "#E1514B",
                    "#F47245",
                    "#FB9F59",
                    "#FEC574",
                    "#FAE38C",
                    "#EAF195",
                    "#C7E89E",
                    "#9CD6A4",
                    "#6CC4A4",
                    "#4D9DB4",
                    "#4776B4",
                    "#5E4EA1"
                ];

                var userId;
                var answersThisUserReceived = [];

                if (user != null) {
                    userId = user.id;
                    for (var i = 0; i < $scope.answers.length; i++) {
                        var answer = $scope.answers[i];
                        if (answer.rId == userId) {
                            answersThisUserReceived.push(answer);
                        }
                    }
                } else {
                    user = {
                        name: "All Users"
                    };
                    answersThisUserReceived = $scope.answers;
                }

                var averageForEachQuestion = {};
                var order = 1;
                for (var i = 0; i < $scope.questions.length; i++) {
                    var question = $scope.questions[i];
                    if (question.type == 'range') {
                        averageForEachQuestion[question.id] = {
                            id: question.id,
                            order: order,
                            score: 0,
                            weight: 1,
                            color: colors[i % colors.length],
                            label: "Question " + question.id,
                            numReceived: 0
                        };
                        order++;
                    }
                }

                for (var i = 0; i < answersThisUserReceived.length; i++) {
                    var answer = answersThisUserReceived[i];
                    if (averageForEachQuestion.hasOwnProperty(answer.qId)) {
                        var averageObj = averageForEachQuestion[answer.qId];
                        averageObj.score += answer.aV;
                        averageObj.numReceived++;
                    }
                }

                var asterData = [];

                for (var i = 0; i < $scope.questions.length; i++) {
                    var question = $scope.questions[i];
                    if (question.type == 'range') {
                        averageForEachQuestion[question.id].score = averageForEachQuestion[question.id].score / averageForEachQuestion[question.id].numReceived;
                        var valuePerPoint = 100 / (question.rangeEnd - question.rangeStart);
                        averageForEachQuestion[question.id].score *= valuePerPoint;
                        averageForEachQuestion[question.id].score = Number(averageForEachQuestion[question.id].score.toFixed(2));
                        asterData.push(averageForEachQuestion[question.id]);
                    }
                }

                var margin = {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 50
                };

                var width = 500,
                    height = 500,
                    radius = Math.min(width, height) / 2,
                    innerRadius = 0.3 * radius;

                var pie = d3.layout.pie()
                    .sort(null)
                    .value(function(d) { return d.width; });

                var tip = d3.tip()
                    .attr('class', 'd3-tip')
                    .offset([0, 0])
                    .html(function(d) {
                        return d.data.label + ": <span style='color:orangered'>" + d.data.score + "</span>";
                    });

                var arc = d3.svg.arc()
                    .innerRadius(innerRadius)
                    .outerRadius(function (d) {
                        return (radius - innerRadius) * (d.data.score / 100.0) + innerRadius;
                    });

                var outlineArc = d3.svg.arc()
                    .innerRadius(innerRadius)
                    .outerRadius(radius);

                d3.select("#d3_report").append("text")
                    .attr("x", (width / 2))
                    .attr("y", 0 - (margin.top / 2))
                    .attr("text-anchor", "middle")
                    .style("font-size", "16px")
                    .style("text-decoration", "underline")
                    .text("Aster Plot for " + user.name);

                var svg = d3.select("#d3_report").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .append("g")
                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                svg.call(tip);

                asterData.forEach(function(d) {
                    d.id     =  d.id;
                    d.order  = +d.order;
                    d.color  =  d.color;
                    d.weight = +d.weight;
                    d.score  = +d.score;
                    d.width  = +d.weight;
                    d.label  =  d.label;
                });
                // for (var i = 0; i < data.score; i++) { console.log(data[i].id) }

                var path = svg.selectAll(".solidArc")
                    .data(pie(asterData))
                    .enter().append("path")
                    .attr("fill", function(d) { return d.data.color; })
                    .attr("class", "solidArc")
                    .attr("stroke", "gray")
                    .attr("d", arc)
                    .on('mouseover', tip.show)
                    .on('mouseout', tip.hide);

                var outerPath = svg.selectAll(".outlineArc")
                    .data(pie(asterData))
                    .enter().append("path")
                    .attr("fill", "none")
                    .attr("stroke", "gray")
                    .attr("class", "outlineArc")
                    .attr("d", outlineArc);


                // calculate the weighted mean score
                var score =
                    asterData.reduce(function(a, b) {
                        //console.log('a:' + a + ', b.score: ' + b.score + ', b.weight: ' + b.weight);
                        return a + (b.score * b.weight);
                    }, 0) /
                    asterData.reduce(function(a, b) {
                        return a + b.weight;
                    }, 0);

                svg.append("svg:text")
                    .attr("class", "aster-score")
                    .attr("dy", ".35em")
                    .attr("text-anchor", "middle") // text-align: right
                    .text(Math.round(score));

            };

            var appendDistributionPlot = function (user, question) {
                var userId;
                var answersThisUserReceived = [];

                if (user != null) {
                    userId = user.id;
                    for (var i = 0; i < $scope.answers.length; i++) {
                        var answer = $scope.answers[i];
                        if (answer.rId == userId) {
                            answersThisUserReceived.push(answer);
                        }
                    }
                } else {
                    user = {
                        name: "All Users"
                    };
                    answersThisUserReceived = $scope.answers;
                }

                var answersForThisQuestion = [];

                if (question != null) {
                    var questionId = question.id;
                    for (var i = 0; i < answersThisUserReceived.length; i++) {
                        answer = answersThisUserReceived[i];
                        if (answer.qId == questionId) {
                            answersForThisQuestion.push(answer);
                        }
                    }
                } else {
                    question = {
                        text: "All Questions"
                    }
                    answersForThisQuestion = answersThisUserReceived;
                }

                var dist = {};

                var myResponses = answersForThisQuestion;

                for (var r = 0; r < myResponses.length; r++) {

                    var response = myResponses[r];

                    dist[response.aV] = dist[response.aV] || 0;
                    dist[response.aV] = parseInt(dist[response.aV]) + 1;
                }

                var plotData = [];

                for (var d in dist) {

                    if (!dist.hasOwnProperty(d)) return;

                    plotData.push({
                        p : dist[d],
                        q : parseInt(d)
                    });
                }

                var plotTitle = "Distribution Plot for " + user.name + " for " + question.text;

                buildBellCurve(plotData, plotTitle);
            };

            function buildBellCurve(d, title) {

                var data = d;

                // getData(); // popuate data

                // line chart based on http://bl.ocks.org/mbostock/3883245
                var margin = {
                        top: 40,
                        right: 20,
                        bottom: 40,
                        left: 50
                    },
                    width = 902 - margin.left - margin.right,
                    height = 500 - margin.top - margin.bottom;

                var x = d3.scale.linear()
                    .range([0, width]);

                var y = d3.scale.linear()
                    .range([height, 0]);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left");

                var line = d3.svg.line()
                    .x(function(d) {
                        return x(d.q);
                    })
                    .y(function(d) {
                        return y(d.p);
                    });

                var svg = d3.select("#d3_report").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                x.domain(d3.extent(data, function(d) {
                    return d.q;
                }));
                y.domain(d3.extent(data, function(d) {
                    return d.p;
                }));

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis);

                svg.append("path")
                    .datum(data)
                    .attr("class", "line")
                    .attr("d", line);

                svg.append("text")
                    .attr("x", (width / 2))
                    .attr("y", 0 - (margin.top / 2))
                    .attr("text-anchor", "middle")
                    .style("font-size", "16px")
                    .style("text-decoration", "underline")
                    .text(title);
            }

            var appendNetworkGraph = function (user, question) {
                var margin = {
                        top: 20,
                        right: 20,
                        bottom: 20,
                        left: 20
                    },
                    width = 902 - margin.left - margin.right,
                    height = 716 - margin.top - margin.bottom;

                var userId;
                var answersThisUserReceived = [];
                var foci = [];

                if (user != null) {
                    userId = user.id;
                    for (var i = 0; i < $scope.answers.length; i++) {
                        var answer = $scope.answers[i];
                        if (answer.rId == userId) {
                            answersThisUserReceived.push(answer);
                        }
                    }
                    foci[userId] = {
                        x: width/2,
                        y: height/2
                    };
                } else {
                    for (var i = 0; i < $scope.users.length; i++) {
                        foci.push({
                            x: (i%8) * 200,
                            y: (Math.floor(i/8) * 200)
                        });
                    }
                    user = {
                        name: "All Users"
                    };
                    answersThisUserReceived = $scope.answers;
                }

                var answersForThisQuestion = [];

                if (question != null) {
                    var questionId = question.id;
                    for (var i = 0; i < answersThisUserReceived.length; i++) {
                        answer = answersThisUserReceived[i];
                        if (answer.qId == questionId) {
                            answersForThisQuestion.push(answer);
                        }
                    }
                } else {
                    question = {
                        text: "All Questions"
                    }
                    answersForThisQuestion = answersThisUserReceived;
                }

                var links = [];
                /*for (var i = 0; i < answersForThisQuestion.length; i++) {
                    links.push({
                        source: i,
                        target:
                    })
                }*/

                var svg = d3.select(".cview").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                //var fill = d3.scale.category10();

                var fill = d3.scale.linear()
                    .domain([0, 1, 2, 3, 4, 5, 6])
                    .range(["red", "orange", "yellow", "white", "lightGreen", "green", "darkGreen"]);

                var force = d3.layout.force()
                    .nodes(answersForThisQuestion)
                    .links(links)
                    .size([width, height])
                    .chargeDistance(80)
                    .linkDistance(6)
                    .start();

                var link = svg.selectAll('.link')
                    .data(force.links())
                    .enter().append('line')
                    .attr('class', 'link');

                var node = svg.selectAll('.node')
                    .data(force.nodes())
                    .enter().append('circle')
                    .attr('class', 'node')
                    .call(force.drag);

                force.on("tick", function (e) {
                    link.attr("x1", function(d) { return d.source.x; })
                        .attr("y1", function(d) { return d.source.y; })
                        .attr("x2", function(d) { return d.target.x; })
                        .attr("y2", function(d) { return d.target.y; });

                    node.attr("r", 5)
                        .style("fill", function(d) { return fill(d.aV); })
                        .style("stroke", function(d) { return d3.rgb(fill(d.aV)).darker(2); })
                        .attr("cx", function(d) { return d.x; })
                        .attr("cy", function(d) { return d.y; });

                    var k = .1 * e.alpha;

                    // Push nodes toward their designated focus.
                    force.nodes().forEach(function(o, i) {
                        o.y += (foci[o.rId].y - o.y) * k;
                        o.x += (foci[o.rId].x - o.x) * k;
                    });
                });
            }
        })

        .controller('LoginCtrl', ['$scope', '$location', 'AuthService', function ($scope, $location, AuthService) {

            /*
            Controller to handle logins.
            In production, logins will be handled by manage.ensighten.com, so this is
            a very basic login system. It uses sessionStorage to store the key, and
            is probably full of holes, but it suffices for now.
             */

            $scope.credentials = {
                account: '',
                username: '',
                password: ''
            };

            $scope.login = function() {

                $scope.credentials.account = document.getElementById('account').value;
                $scope.credentials.username = document.getElementById('username').value;
                $scope.credentials.password = document.getElementById('password').value;

                AuthService.login($scope.credentials)
                    .then(function() {
                        $location.path('/');
                    }, function() {

                        // TODO: add a login failed message on the UI.

                    });
            };
        }])

        .controller('NavigationCtrl', ['$scope', '$location', 'AuthService', 'Utils', function ($scope, $location, AuthService, Utils) {

            function getToggleState(state) {

                if (typeof $scope.toggleState[state] == 'undefined') {

                    $scope.toggleState[state] = false; // default is false for all toggle states

                    Utils.saveToStorage('ens-mobile-navbar-toggleState', $scope.toggleState, 'local');

                    return false;

                } else {

                    return $scope.toggleState[state];
                }
            }

            function setToggleState(state, force) {

                getToggleState(state);

                if (typeof force !== 'undefined') {

                    $scope.toggleState[state] = force;

                } else {

                    $scope.toggleState[state] = !$scope.toggleState[state];
                }

                Utils.saveToStorage('ens-mobile-navbar-toggleState', $scope.toggleState, 'local');
            }

            function updateTab() {

                $scope.tab = $location.path().replace(/(?:\/mobile)?\/([^\/]*).*/i, '$1'); // regex to find tab from URL
            }

            function hasPermission(stringPersmissionName) {

                if (!stringPersmissionName) return;

                var permissionsFromSession = AuthService.getSessionDetails().rolePermissions.split(',');

                // ui hack to show optimization beta for devs, mobile_training and clientId 130

                // if (stringPersmissionName == 'view mobile optimization') {

                //     var clientId = AuthService.getSessionDetails().account.toLowerCase();
                //     var allowedClientId = ['mobile_training', 'homedepotmobile', 'thehomedepot'];

                //     return allowedClientId.indexOf(clientId) > -1;
                // }

                return permissionsFromSession.indexOf(stringPersmissionName) > -1;
            }

            // store, get and set toggle states

            $scope.toggleState = JSON.parse(Utils.getFromStorage('ens-mobile-navbar-toggleState', 'local')) || {};
            // $scope.toggleState = {}; // switch of remembering toggle options

            $scope.getToggleState = getToggleState;
            $scope.setToggleState = setToggleState;

            $scope.hasPermission = hasPermission;

            // update selected nav item

            $scope.$on('$routeChangeSuccess', updateTab);
        }])

        .controller('HeaderCtrl', ['$scope', '$location', 'AuthService', function($scope, $location, AuthService, ProductsService, SocketService) {

            var session = AuthService.getSessionDetails();

            // all available permissions for the user

            $scope.account = 'Saravanan Ganesh';
        }])

        .controller('FooterCtrl', ['$scope', '$location', function($scope, $location) {

            $scope.year = (new Date()).getFullYear();
        }])

});
