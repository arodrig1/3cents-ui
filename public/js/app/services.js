define([

    'angular',
    'app/constants',
    'moment'

], function(angular, constants, moment) {

    'use strict';

    var debug = false;

    return angular.module('mobileUI.services', [])

        .factory('DashboardService', function($rootScope, $q, $http, AuthService) {

            // prepare components

            var nextSurveyInstance = null;

            var getNextDeadline = function() {

                var deferred = $q.defer();

                $http.get('/api/si/open')
                    .success(function(response) {

                        nextSurveyInstance = response['surveyInstances']['responses'][0];

                        var deadlineInEpoch = response['surveyInstances']['responses'][0]['surveyDeadline'];
                        var deadlineInDueIn = moment(deadlineInEpoch, 'x').fromNow();
                        var deadlineInDate = moment(deadlineInEpoch, 'x').format('MMMM Do YYYY');

                        var surveyInstanceFirstDeadline = {

                            'dueIn': deadlineInDueIn,
                            'date': deadlineInDate
                        }

                        deferred.resolve(surveyInstanceFirstDeadline);
                    })

                return deferred.promise;
            }

            var getSurveyInstanceWithId = function() {

                var deferred = $q.defer();

                if (!nextSurveyInstance.id) deferred.reject({});

                $http.get('/api/si/' + nextSurveyInstance.id) // using nextSurveyInstance.id
                    .success(function(response) {

                        return deferred.resolve(response['surveyInstance']);
                    })

                return deferred.promise;
            }

            var getSurveyInstanceRecipientsWithId = function() {

                var deferred = $q.defer();

                $http.get('/api/si/' + nextSurveyInstance.id + '/recipients/users/' + AuthService.getUserId())
                    .success(function(response) {

                        return deferred.resolve(response['surveyInstance']['recipients']);
                    })

                return deferred.promise;
            }

            var getSurveyInstanceUsersWithId = function() {

                var deferred = $q.defer();

                $http.get('/api/si/' + nextSurveyInstance.id + '/users/' + AuthService.getUserId())
                    .success(function(response) {

                        return deferred.resolve(response['UserListResponse']['users']);
                    })

                return deferred.promise;
            }

            var newRecipientForSurveyInstanceWithId = function(userId) {

                var deferred = $q.defer();

                var data = {

                    'sId': nextSurveyInstance.id,
                    'rId': userId,
                    'uId': AuthService.getUserId(),
                    'wasR': false
                }

                $http.get('/api/add/' + data.sId + '/' + data.rId + '/' + data.uId + '/' + data.wasR)
                    .success(function(response) {

                        return deferred.resolve(response);
                        // return deferred.resolve(response['request']);
                    })

                return deferred.promise;
            }

            var newProviderForSurveyInstanceWithId = function(userId) {

                var deferred = $q.defer();

                var data = {

                    'sId': nextSurveyInstance.id,
                    'rId': AuthService.getUserId(),
                    'uId': userId,
                    'wasR': true
                }

                $http.get('/api/add/' + data.sId + '/' + data.rId + '/' + data.uId + '/' + data.wasR)
                    .success(function(response) {

                        return deferred.resolve(response);
                        // return deferred.resolve(response['request']);
                    })

                return deferred.promise;
            }

            // prepare the object

            var DashboardService = {};

            DashboardService.getNextDeadline = getNextDeadline;
            DashboardService.getNextSurveyInstance = getSurveyInstanceWithId;
            DashboardService.getNextSurveyInstanceRecipients = getSurveyInstanceRecipientsWithId
            DashboardService.getNextSurveyInstanceUsers = getSurveyInstanceUsersWithId;
            DashboardService.newRecipientForSurveyInstance = newRecipientForSurveyInstanceWithId;
            DashboardService.newProviderForSurveyInstanceWithId = newProviderForSurveyInstanceWithId;

            // release

            return DashboardService;
        })

        .factory('AdminService', function($rootScope, $q, $http) {

            // prepare components

            var getNextSurveyInstanceData = function(nextSurveyInstanceId) {

                var deferred = $q.defer();

                // --------------------------------------------------------
                ///////////////////////////////////////////////////////////

                $http.get('/api/si/id/data.json') // CHANGE DATA HERE
                    .success(function(response) {

                        deferred.resolve(response['surveyInstance']);
                    })

                return deferred.promise;
            }

            // prepare the object

            var AdminService = {};

            AdminService.getNextSurveyInstanceData = getNextSurveyInstanceData;

            // release

            return AdminService;
        })

        .factory('SurveyService', function($rootScope, $q, $http) {

            // prepare components

            var getInstanceById = function(surveyInstanceId) {

                var deferred = $q.defer();

                $http.get('/api/si/' + surveyInstanceId)
                    .success(function(response) {

                        deferred.resolve(response['surveyInstance']);
                    })

                return deferred.promise;
            }

            var getSurveyById = function(surveyId) {

                var deferred = $q.defer();

                $http.get('/api/si/survey/' + surveyId)
                    .success(function(response) {

                        deferred.resolve(response['survey']);
                    })

                return deferred.promise;
            }

            var getInstanceAnswersById = function(surveyId) {

                var deferred = $q.defer();

                $http.get('/api/si/id.json')
                    .success(function(response) {

                        deferred.resolve(response['surveyInstance']);
                    })

                return deferred.promise;
            }

            var saveAnswersForSurveyInstance = function(data) {

                var deferred = $q.defer();

                $http.put('/api/si/' + data.surveyInstanceId + '/recipient/' + data.recipientId + '/answers/user/' + data.userId, data)
                    .success(function(response) {

                        console.log('ufff');
                        deferred.resolve({});
                    })

                return deferred.promise;
            }

            // prepare the object

            var SurveyService = {};

            SurveyService.getSurveyById = getSurveyById;
            SurveyService.getInstanceById = getInstanceById;
            SurveyService.saveAnswersForSurveyInstance = saveAnswersForSurveyInstance;

            // release

            return SurveyService;
        })

        .factory('UserService', function($rootScope, $q, $http) {

            // prepare components

            var getUserById = function(userId) {

                var deferred = $q.defer();

                $http.get('/api/user/' + userId)
                    .success(function(response) {

                        deferred.resolve(response['user']);
                    })

                return deferred.promise;
            }

            var getUsersByOrgId = function(orgId) {


            }

            // prepare the object

            var UserService = {};

            UserService.getUserById = getUserById;

            // release

            return UserService;
        })

        /*
        *
        * ! Auth Service
        *
        */
        .factory('AuthService', function($http, Session, $cookies) {

            // ------------------------------------------------------------------------
            ///////////////////////////////////////////////////////////////////////////

            var userId = '1'; // CHANGE USER ID

            // ------------------------------------------------------------------------
            ///////////////////////////////////////////////////////////////////////////

            return {

                getUserId: function() {

                    return userId;
                },

                setUserId: function(u) {

                    userId = u;
                },

                login: function(credentials) {

                    var requestObj = this.createLoginRequestObject(credentials);
                    return $http({
                            method: 'POST',
                            url: Constants.AUTH_ENDPOINT,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: requestObj
                        })
                        .then(function(res) {
                            var sessionKey = res.data.responseData.sessionKey;
                            // Session.create(sessionKey, credentials.username, credentials.account);
                        });
                },

                createLoginRequestObject: function(credentials) {
                    var reqObj = {
                        "command": "login",
                        "parameters": [{
                            "paramName": "username",
                            "paramValue": credentials.username
                        }, {
                            "paramName": "password",
                            "paramValue": credentials.password
                        }, {
                            "paramName": "account",
                            "paramValue": credentials.account
                        }, {
                            "paramName": "appid",
                            "paramValue": "mobileui"
                        }, {
                            "paramName": "appname",
                            "paramValue": "mobileui"
                        }]
                    };
                    return 'data=' + encodeURIComponent(JSON.stringify(reqObj)).replace(/%20/g, '+');
                },

                isAuthenticated: function() {

                    // restore via cookie
                    if ($cookies.apiKey) {
                        Session.create($cookies.apiKey, $cookies.username, $cookies.account, $cookies.rolePermissions);
                        return true;
                    }

                    // Look for session key.
                    if (Session.sessionKey || sessionStorage.sessionKey) {
                        return true;
                    }

                    return false;
                },

                getSessionDetails: function() {

                    return {
                        sessionKey: sessionStorage.sessionKey || $cookies.apiKey,
                        username: sessionStorage.username || $cookies.username,
                        account: sessionStorage.account || $cookies.account,
                        rolePermissions: sessionStorage.rolePermissions || $cookies.rolePermissions
                    };
                }
            }
        })

        /*
        *
        *   ! Utils
        *
        */
        .factory('Utils', [function() {
            var Utils = {};
            Utils.qualifyURL = function(url) {

                if (url == '/images/tinyArrow.png') {

                    // console.log('From Utils.qualifyURL() : ' + '/mobile' + url);
                }

                if (document.location.hostname != 'localhost') {

                    return '/mobile' + url;
                }

                return url;

            }

            Utils.convertToLetter = function(zeroIndexNumber) {

                if (zeroIndexNumber > 25) zeroIndexNumber = 8669; // show infinity :P
                return String.fromCharCode(65 + Number(zeroIndexNumber));
            }

            Utils.parseURL = function(url) {
                // Attribution to http://www.php.net/manual/en/function.parse-url.php#104958
                var splitUrlRegexp = /^(([^:\/?#]+):)?((\/\/)?([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/;
                var urlParts;
                var retObj = {};
                try {
                    urlParts = url.match(splitUrlRegexp);
                } catch (e) {}

                // If it was successfully broken down
                if (urlParts) {
                    // Get the host and filepath
                    retObj.protocol = urlParts[2] || '';
                    retObj.host = urlParts[5] || '';
                    retObj.hashFragment = urlParts[10] || '';
                    retObj.filePath = '';
                    retObj.file = '';
                    retObj.path = urlParts[6];
                    retObj.queryStr = urlParts[8] || '';
                    retObj.queryParams = (!!urlParts[8]) ? urlParts[8].split('&') : [];
                    var paramMap = {};
                    for (var i = 0; i < retObj.queryParams.length; i++) {
                        var param = retObj.queryParams[i],
                            idx = param.indexOf('='),
                            name = param.substr(0, idx),
                            val = decodeURIComponent(param.substr(idx + 1));
                        paramMap[name] = val;
                    }
                    retObj.queryParams = paramMap;
                    // Find the last slash
                    var slashIndex = retObj.path.lastIndexOf('/');

                    // If one exists, cut there
                    if (slashIndex !== -1) {
                        retObj.filePath = (slashIndex > 0) ? retObj.path.substring(0, slashIndex + 1) : '/';
                        retObj.file = retObj.path.substring(slashIndex + 1);
                    } else {
                        // Otherwise, the path is empty and the file is the last piece
                        retObj.file = retObj.path;
                    }
                }

                return retObj;
            };
            Utils.saveToStorage = function(key, value, type) {

                if (typeof value === 'object' || typeof value === "array") {
                    value = JSON.stringify(value);
                }

                if (type && type === 'local') {
                    localStorage.setItem(key, value);
                } else {
                    sessionStorage.setItem(key, value);
                }
            };
            Utils.getFromStorage = function(key, type) {
                if (type && type === 'local') {
                    if (localStorage.getItem(key)) {
                        return localStorage.getItem(key);
                    }
                    return null;
                } else {
                    if (sessionStorage.getItem(key)) {
                        return sessionStorage.getItem(key);
                    }
                    return null;
                }
            };
            Utils.removeFromStorage = function(key, type) {
                if (type && type === 'local') {
                    localStorage.removeItem(key);
                } else {
                    sessionStorage.removeItem(key);
                }
            };
            Utils.removeComments = function(code) {

                var c = new CommentStripper();

                // console.log(c.strip('//hello world \n var a;'));

                return c.strip(code);
            };

            Utils.dateForHumans = function(isoDate, format) {

                if (!format) format = 'YYYY-MM-DD'; //default format

                var d = new Date(parseInt(isoDate));

                var dateIn = {};

                dateIn['YYYY-MM-DD'] = d.getFullYear() + "-" + ('00' + (d.getMonth() + 1)).slice(-2) + "-" + ('00' + d.getDate()).slice(-2);
                dateIn['dateString'] = d.toDateString();

                return dateIn[format];
            };

            return Utils;
        }])

        /*
        *
        *   ! Session
        *
        */
        .service('Session', [function() {
            this.create = function(sessionKey, username, account, rolePermissions) {

                this.sessionKey = sessionKey;
                this.username = username;
                this.account = account;
                this.rolePermissions = rolePermissions;
                sessionStorage.sessionKey = sessionKey;
                sessionStorage.username = username;
                sessionStorage.account = account;
                sessionStorage.rolePermissions = rolePermissions;
            };
            this.destroy = function() {

                this.sessionKey = null;
                this.username = null;
                this.account = null;
                this.rolePermissions = null;
                sessionStorage.removeItem('sessionKey');
                sessionStorage.removeItem('username');
                sessionStorage.removeItem('account');
                sessionStorage.removeItem('rolePermissions');
            };
            return this;
        }])


        // CONTAINS YET TO BE IMPLEMENTED API END POINTS

        .factory('UsersService', function($rootScope, $q, $http) {

            var getUsersBySurveyInstanceId = function(surveyInstanceId) {

                var deferred = $q.defer();

                $http.get('/api/users/si/' + surveyInstanceId)
                    .success(function(response) {

                        if (response['users'])
                            deferred.resolve(response['users']);

                        else deferred.reject({'error' : {'reason' : 'Users Response is not structured.'}});
                    })

                return deferred.promise;
            }

            var getRecipientsBySurveyInstanceId = function(surveyInstanceId) {

                var deferred = $q.defer();

                $http.get('/api/users/si/' + surveyInstanceId + '/recipients')
                    .success(function(response) {

                        if (response['users'])
                            deferred.resolve(response['users']);

                        else deferred.reject({'error' : {'reason' : 'Users Response is not structured.'}});
                    })

                return deferred.promise;
            }

            return {

                getUsersBySurveyInstanceId : getUsersBySurveyInstanceId,
                getRecipientsBySurveyInstanceId : getRecipientsBySurveyInstanceId
            }
        })

        .factory('SurveyServicee', function($rootScope, $q, $http) {
        })

        .factory('SurveyInstanceService', function($rootScope, $q, $http) {

            // prepare functions

            var getSurveyInstancesByStatus = function(status) {

                var deferred = $q.defer();

                if (['open', 'closed'].indexOf(status) == -1) deferred.reject({'error' : {'reason' : 'Invalid Status'}})

                $http.get('/api/si/status/' + status)
                    .success(function(response) {

                        if (response['surveyInstances'])
                            deferred.resolve(response['surveyInstances']);

                        else deferred.reject({'error' : {'reason' : 'Survey Instances Response is not structured.'}});
                    })

                return deferred.promise;
            };

            var getSurveyInstanceById = function(surveyInstanceId) {

                var deferred = $q.defer();

                $http.get('/api/si/' + surveyInstanceId)
                    .success(function(response) {

                        if (response['surveyInstance'])
                            deferred.resolve(response['surveyInstance']);

                        else deferred.reject({'error' : {'reason' : 'Survey Instance Response is not structured.'}});
                    })

                return deferred.promise;
            };

            // pack functions into object and return

            return {

                getSurveyInstancesByStatus : getSurveyInstancesByStatus,
                getSurveyInstanceById : getSurveyInstanceById
            };
        })

        .factory('AnswersService', function($rootScope, $q, $http) {

            var getAnswersBySurveyInstanceId = function(surveyInstanceId) {

                var deferred = $q.defer();

                $http.get('/api/answers/si/' + surveyInstanceId)
                    .success(function(response) {

                        if (response['answers'])
                            deferred.resolve(response['answers']);

                        else deferred.reject({'error' : {'reason' : 'Answers Response is not structured.'}});
                    })

                return deferred.promise;
            };

            return  {

                getAnswersBySurveyInstanceId : getAnswersBySurveyInstanceId
            }
        });
});
