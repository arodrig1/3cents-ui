define([

    'angular',
    'angucomplete-alt',
    'angularCookies',
    'angularRoute',
    'app/controllers',
    'app/directives',
    'app/filters',
    'app/services',
    'd3',
    'jquery',
    'moment'

], function (angular) {

    'use strict';

    return angular.module('mobileUI', [

        'mobileUI.controllers',
        'mobileUI.directives',
        'mobileUI.filters',
        'mobileUI.services',
        'ngCookies',
        'ngRoute'

    ]).run(['$rootScope', '$location', '$window', 'AuthService', function($rootScope, $location, $window, AuthService) {

        $rootScope.$on('$locationChangeStart', function(event, next, current) {

            if (!AuthService.isAuthenticated()) {

                switch($location.$$host) {

                    case 'localhost':
                        $location.path('/login');
                        break;

                    default:
                        $window.location.href = '//' + $location.$$host + '/';
                        break;
                }
            }
        });

    }]);
});
