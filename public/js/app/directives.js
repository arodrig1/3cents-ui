define([

    'angular',
    'app/constants'

], function (angular, Constants) {

    'use strict';

    return angular.module('mobileUI.directives', [])


        .directive('ensPilotMenu', function () {
            return {
                restrict : 'E',
                templateUrl : 'templates/directives/pilot-menu.html',
                replace : true
            };
        })

        .directive('ensObMenu', function () {
            return {
                restrict:'A',
                templateUrl: 'templates/directives/ob-menu.html',
                replace: true
            };
        })

        .directive('ensObList', function () {
            return {
                restrict: 'A',
                templateUrl: 'templates/directives/ob-list.html',
                replace: true
            };
        })

        .directive('ensObSave', function() {
            return {
                restrict: 'A',
                templateUrl: 'templates/directives/ob-save.html',
                replace: true
            };
        })

        .directive('ensConnectionInfo', function () {
            return {
                restrict : 'E',
                templateUrl : 'templates/directives/connection-info.html',
                replace : true
            };
        })

        .directive('ensConsole', function() {
            return {
                restrict: 'E',
                templateUrl: 'templates/directives/console.html',
                replace: true
            }
        })

        .directive('ensSocketDisconnected', function() {
            return {
                restrict: 'E',
                templateUrl: 'templates/directives/not-connected.html',
                replace: true
            }
        })

        .directive('ensDeviceNotConnected', function() {
          return {
            restrict: 'E',
            templateUrl: 'templates/directives/device-not-connected.html',
            replace: true
          }
        })

        .directive('ensAutoscroll', function() {
            return {
                link: function postLink(scope, element, attrs) {

                    scope.$watch( function () {

                            return element.prop('scrollHeight') + element.prop('clientHeight');

                        }, function () {

                                element.prop('scrollTop', element.prop('scrollHeight') );
                        }
                    );
                }
            }
        })

        .directive('ensResizer', function($document) {

            return function(scope, element, attrs) {

                element.on('mousedown', function(event) {

                    event.preventDefault();

                    if(event.which != 1) {
                        return;
                    }

                    $document.on('mousemove', mousemove);
                    $document.on('mouseup', mouseup);
                });

                function mousemove(event) {

                    if (attrs.ensResizer == "vertical") {

                        var y = window.innerHeight - event.pageY;
                        var d = parseInt(attrs.ensResizerDistance) || 0;

                        if(attrs.ensResizerTop) {

                            document.querySelector(attrs.ensResizerTop)
                                .style.bottom = (y - d) + 'px';
                        }

                        if(attrs.ensResizerBottom) {

                            if((y + d < parseInt(attrs.ensResizerMin))
                                || (y + d > parseInt(attrs.ensResizerMax))) {
                                return;
                            }

                            var a = document.querySelector(attrs.ensResizerBottom);

                            document.querySelector(attrs.ensResizerBottom)
                                .style.height = (y + d) + 'px';

                            // save height of the console to the service

                            scope.devicesService.heightConsole = (y + d) + 'px';
                        }


                    } else if (attrs.ensResizer == "horizontal") {

                        // to be developed

                    }
                }

                function mouseup(event) {

                    scope.devicesService.saveConsoleHeight();

                    $document.unbind('mousemove', mousemove);
                    $document.unbind('mouseup', mouseup);
                }
            }
        })

        /*
        *
        * ! ens-render
        *
        * @usage: <div ens-render="" ... and several attributes> </div>
        *
        *
        * takes in a JSON dump from the device, and renders in beautiful HTML
        *
        * to walkthrough building a render, start at fetchDump(screenDumpId);
        *
        */
        .directive("ensRender", function () {

            return {
                restrict: "A",
                scope: {

                    "abt": "=abt",
                    "dumpSource": "=dumpsource",
                    "canClick": "=canclick",
                    "properties": "=properties",
                    "history": "=history",
                    "change": "=change",
                    "config": "=config",
                    "changed": "=changed",
                    "zoomed": "=zoomed",
                    "width": "=width",
                    "historyIndex":"=historyIndex",
                    "editInProgress": "=editInProgress"
                },
                controller: function ($scope, $element, $window, $rootScope, $timeout, $interval, $animate, DumpService, CurrentPlatform, ABConnectService, $compile) {

                    var debug = false;

                    $scope.platform = CurrentPlatform.platform;

                    // While one could continually reference the singleton CurrentPlatform it seemed
                    // a bit easier to follow the previous conventions of $scope.platform
                    // and just set it here.

                    if (debug) window.render = $scope;

                    $scope.hashMap = {};
                    $scope.downScale = 0.5;
                    $scope.deviceScale = 2;
                    $scope.dump = undefined;
                    $scope.variation = undefined; // ?
                    $scope.lockSelectedFromList = false;
                    $scope.properties = [];
                    $scope.renderComplete = false;

                    $scope.renderElement = null;
                    $scope.selected = null;
                    $scope.selectedProperties = null; // how is this different from $scope.properties ?
                    $scope.show = null;

                    $scope.noId = 0;

                    $scope.showHistory = null;
                    $scope.tabLevel = 0;

                    $scope.select = {

                        element : null,
                        property : null
                    };

                    /*
                    *
                    * Watch Dump Source (dump screen id)
                    *
                    */
                    $scope.$watch("dumpSource", function (newVal, oldVal) {

                        if ((!newVal || !oldVal) || !(newVal.length < 1 || oldVal.length < 1)) {

                            if ($scope.$parent.fetch) // what does $scope.$parent.fetch refer to?
                                fetchDump(newVal);

                            applyChangesToScope(true);
                            refreshAfterChange('selected'); // must remove selected as parameter, because selected did not change

                            if(debug) console.log("new : %s, old : %s", newVal, oldVal);
                        }
                    });

                    /*
                    *
                    * Watch Properties Array
                    *
                    * if it was cleared, clear the selection
                    *
                    * need to find a way to properly trigger a clear selection
                    * inside the directive from the outer scope.
                    *
                    */
                    $scope.$watch("properties", function (newVal, oldVal) {

                        // if there was no old value, return

                        if (!oldVal || oldVal.length < 1) return;

                        // if there is a new value and it was cleared, clear selection

                        if (newVal && newVal.length == 0) clearSelection();
                    });

                    /*
                    *
                    * Watch Change Property (The Latest Edit)
                    *
                    * apply edits to scope
                    * refresh render
                    *
                    * need to find a way to properly trigger a clear selection
                    * inside the directive from the outer scope.
                    *
                    */
                    $scope.$watch("change", function (newVal, oldVal) {

                        applyChangesToScope();
                        refresh();
                        refreshAfterChange('selected');
                    });

                    $scope.$watch('zoomed', function(newVal) {

                        refreshAfterChange('view');
                        refreshAfterChange('selected');
                    });

                    /*
                    *
                    * ! applyChangesToScope()
                    * RENAMED FROM applyEditedChanges()
                    *
                    * Function to apply changes from render refreshes and property edits
                    * on to the ensRender scope variables
                    *
                    * call refresh() to apply these changes to the render.
                    *
                    */
                    function applyChangesToScope (refreshWhenDone) {

                        // if render isn't complete, try again after 500 ms

                        if (!$scope.renderComplete) {

                            $timeout(function () {

                                applyChangesToScope(refreshWhenDone);
                                if ( !$scope.$phase ) $scope.$apply(); // ?

                            }, 500);

                            return;
                        }

                        // if there are no changes, return

                        if (!$scope.change || !$scope.change["name"] ) return;

                        // if they don't already exist, update selected / selectedProperties based on hashID in the change

                        if (!$scope.selectedProperties || !$scope.selected || $scope.selected.id != $scope.change.hashID) {

                            $scope.selected = document.getElementById($scope.change.hashID);
                            $scope.selectedProperties = angular.copy($scope.hashMap[$scope.change.hashID]);
                        }

                        var index = $scope.history.length-1;
                        var length = $scope.history.length;

                        // go through each change item form history to bring render / hashMap / properties to the right state

                        applyChangeItemToScope ($scope.history[index], index, length, $scope.historyIndex); // why not just call the last item?

                        if (refreshWhenDone) refresh(); // shouldn't this be called anyway?
                    }

                    /*
                    *
                    * ! applyChangeItemToScope(historyItem, index, length, activeIndex)
                    *
                    * recursive - for all changes in the reverse order to ensure the latest change is the last one applied.
                    * this is the reverse of clearing history where the natural order is used to ensure the default value is the last one applied.
                    *
                    */
                    function applyChangeItemToScope (historyItem, index, length, activeIndex) {

                        // if this history item is valid and not been undone, it must be applied

                        if (activeIndex - 1 < index && index < length) {

                            if (debug) console.log("Applying change[%s] for %s to %s", index, historyItem.name, historyItem.value );

                            var historyItemValue = null;

                            // if the history item is an advanced item, generate advanced value object

                            if (historyItem.name === 'advanced')  {

                                historyItemValue = {

                                    value : historyItem.value,
                                    propName : historyItem.propName,
                                    propType : historyItem.propType,
                                    qualifiers : historyItem.qualifiers
                                };

                            } else {

                                historyItemValue = historyItem.value;
                            }

                            // When the change matches the currently
                            // selected element, update its properties

                            // if not?

                            if (historyItem.hashID == $scope.change.hashID) {

                                // $scope.selectedProperties[historyItem.name] = historyItem.value; //STODO : use historyItemValue
                                $scope.selectedProperties[historyItem.name] = historyItemValue;
                            }

                            // Update data element // what does this do?

                            // $scope.hashMap[historyItem.hashID][historyItem.name] = historyItem.value; //STODO : use historyItemValue
                            $scope.hashMap[historyItem.hashID][historyItem.name] = historyItemValue;

                            // iOS nuance.
                            // update text property if attributed text changes.

                            // why? can't the render obey attributed text or not show attributed text?

                            if (historyItem.name == 'attributedText') {

                                $scope.hashMap[historyItem.hashID]['text'] = historyItem.value;
                            }

                            generatePropertyItems();

                            applyChangeItemToScope($scope.history[index - 1], index - 1, length, activeIndex); // recursively apply the next item

                        } else { // all done

                            refresh();
                        }
                    }

                    /*
                    *
                    * ! fetchDump(_var)
                    *
                    * @param {_var} : ?
                    *
                    *
                    */
                    function fetchDump(_var) {

                        $element[0].id += "variation-" + _var;
                        $scope.variation = _var;

                        DumpService.fetch(_var, function (newDump) {

                            if ($scope.changed && _var == 'live') return; // if there are changes made do not refresh dump from **LIVE** fetch service
                            if (!newDump) return;

                            // TODO : Call refreshIfReady() instead to avoid flicker.
                            $scope.dump = newDump;
                            refresh();
                            refreshAfterChange('view');

                        });
                    }

                    /* ! Render - Configure Elements */
                    function prepDataByPlatform (data) {

                        for ( var dumpKey in Constants.DUMP_PREP[$scope.platform] ) {

                            var prepFunction = Constants.DUMP_PREP[$scope.platform][dumpKey];
                            data[dumpKey] = prepFunction (data, $scope.platform, $scope);

                        }

                        return data;
                    }

                    /*
                    *
                    * ! prepElementInfo
                    *
                    * called for each element in the dump, to edit properties platform wise. \
                    * TODO: MUST move to a seperate step that being baked into rendering the dump into HTML.
                    *
                    */
                    function prepElementInfo(data) {

                        return prepDataByPlatform(data);
                    }

                    function parseSubviewToHTML (elSubview, subview) {

                        var debug = debug || false;

                        // iterate over a list of functions to set div styles

                        for (var htmlKey in Constants.DIV_STYLES[$scope.platform]) {

                            var parsingFunction = null;
                            var parsedStyle = null;

                            // second nested iteration for "style" attributeee

                            if (htmlKey == 'style') {

                                for (var style in Constants.DIV_STYLES[$scope.platform].style) {

                                    // each key has its own function for parsing

                                    parsingFunction = Constants.DIV_STYLES[$scope.platform].style[style];
                                    parsedStyle = parsingFunction(elSubview, subview, $scope.platform, $scope);

                                    if (debug) console.log("Setting el.style.%s to %s for %s", style, parsedStyle, subview.class);

                                    elSubview.style[style] = parsedStyle;
                                }

                            } else if (htmlKey == 'src' || htmlKey == 'background' || htmlKey.indexOf('drawable') == 0) {

                                parsingFunction = Constants.DIV_STYLES[$scope.platform][htmlKey];
                                var elWithImageChild = parsingFunction(elSubview, subview, $scope.platform, $scope);

                                if (debug) console.log("Setting el.%s to %s for %s", htmlKey, elWithImageChild, subview.class);

                                elSubview = elWithImageChild;

                            } else {

                                // First level contains things such as "src",
                                // "zIndex" & "innerHTML"

                                parsingFunction = Constants.DIV_STYLES[$scope.platform][htmlKey];
                                var parsedHTML = parsingFunction(elSubview, subview, $scope.platform, $scope);

                                if (debug) console.log("Setting el.%s to %s (className was %s) for %s", htmlKey, parsedHTML, elSubview.className, subview.class);

                                elSubview[htmlKey] = parsedHTML;
                            }
                        }

                        return elSubview;
                    }

                    function configureViewStyle(elSubview, subview) {

                        elSubview = parseSubviewToHTML( elSubview, subview, $scope.platform, $scope );
                        setDragability(elSubview, true);
                        // setResizability(elSubview, true);

                        return elSubview;
                    }

                    function buildDivider(parentObj, bottomObj) {

                      //this is getting overwritten
                       // var data = parentObj;

                      var data = {};

                      data.subviews = {};

                     var image = {};
                     // Constants.MOD_PROPS.android.src.type = 'image';

                     if (parentObj['dividerImage']) {
                        if (parentObj['dividerImage'].imageData) {

                           var d = parentObj['dividerImage'];
                           var imageType = d['imageType'];
                           // var imageData = new Image(buttonRect.width, buttonRect.height);
                           // imageData.src = "data:" + imageType + ";base64," + data['image'];
                           if (d['imageData'].toString().indexOf("data:") === 0)
                              image['imageData'] = d['imageData'];
                           else
                              image['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                           image['imageType'] = imageType;
                           //image['imageSize'] = ( data.image.imageSize ) ? data.image.imageSize : 1;
                           data.image = image;

                        } else {

                           var url = parentObj.image;
                           data.image = {}
                           data.image.imageData = url;
                        }
                     }

                      data.rect = {};
                      data.rect.height = ( parentObj['dividerHeight'] ) ? parentObj['dividerHeight'] : 1
                      // data.rect.height = ( Number(data.rect.height) < 1 ) ? 1 : data.rect.height;
                      data.rect.width = parentObj.rect.width
                      data.rect.x = parentObj.rect.x;
                      data.rect.y = bottomObj.rect.y - data.rect.height;

                      console.log("Setting divider height at " + data.rect.height);

                      data.background = {};
                      // Constants.MOD_PROPS.android.background.type = 'color';
                      if(parentObj['dividerColor'])
                      {
                         data.background = parentObj['dividerColor']
                      }
                      else
                      {
                         data.background.r = 0;
                         data.background.g = 0;
                         data.background.b = 0;
                         data.background.a = 1;
                      }



                      data.visibility = parentObj.visibility;
                      return data;
                    }

                    function cleanElementInfo(obj) {

                        var removePropertyList = [

                            'subviews', 'scaledRect'
                        ];

                        for (var a in obj) {

                            if (removePropertyList.indexOf(a.toString()) > -1)
                                delete obj[a];
                        }

                        if (obj.image && obj.image.imageData) {

                            obj.image.imageData = obj.image.imageData.substring(0, 30) + '...';
                        }

                        setEnumeration(obj, "show", false, 0);

                        return obj;
                    }

                    function addSubView(view, subview) {

                        // do not add subview to dom if it is hidden
                        if ($scope.platform == "android") {
                            if (subview.visibility == Constants.ANDROID.VIEW.GONE) {
                                // console.log("Hiding item with visibility ( %s vs %s )", subview.visibility, Constants.ANDROID.VIEW.GONE);
                                return view;
                            }
                        }
                        else {
                            if (subview.hidden === "yes" || Number(subview.alpha) < 0.1) {
                                return view;
                            }
                        }

                        // build the object to be put in the list // move it to a function
                        var copy = JSON.parse(JSON.stringify(subview));
                        copy = cleanElementInfo(copy);
                        // setEnumeration(copy, "tabbedName", $scope.tabString, 0);

                        $scope.list.push(copy);

                        // create the DOM element
                        var tagName = 'div';
                        if ($scope.platform == 'ios' && subview.text)
                            tagName = 'p';

                        var elSubview = document.createElement(tagName);
                        elSubview = configureViewStyle(elSubview, subview);

                        // if (tagName == 'img') {
                        //     var elWrapper = document.createElement('div');
                        //     elWrapper.className = 'view';
                        //     elWrapper.appendChild(elSubview);
                        //     view.appendChild(elWrapper);

                        // } else {

                        view.appendChild(elSubview);
                        // }


                        // $scope.tabString += $scope.tabCharacter;
                        $scope.tabLevel++;

                        for (var iterateSubViewsOfSubViews = 0; iterateSubViewsOfSubViews < subview.subviews.length; iterateSubViewsOfSubViews++) {

                           if($scope.platform == "android" && subview.class.match("android.widget.ListView") && iterateSubViewsOfSubViews >= 1)
                           {

                             //build divider
                             //add subview
                             $scope.tabLevel++;
                              var divider = buildDivider(subview, subview.subviews[iterateSubViewsOfSubViews])
                              elSubview = addSubView(elSubview, divider);
                              $scope.tabLevel--;
                           }
                            var next = prepElementInfo(subview.subviews[iterateSubViewsOfSubViews]);
                            addSubView(elSubview, next);
                        }

                        // $scope.tabString = $scope.tabString.substring(0, $scope.tabString.length - 3);
                        $scope.tabLevel--;

                        return view;
                    }

                    /*
                    *
                    * ! refresh()
                    *
                    * refreshes the render DOM, after changes to data / selected
                    *
                    */
                    function refresh() {

                        // if no dump exists, return

                        if (!$scope.dump) { // return if not dump available

                            if (debug) console.log("No current dump available");

                            $scope.dumpName = "Waiting...";
                            return;
                        }

                        $scope.viewControllers = {}; // ?

                        $scope.noId = 0;
                        $scope.list = [];
                        $scope.hashMap = {}; // ?

                        // create an element to append all screens
                        // TODO: cleanup only one screen is appended every time

                        var screen = document.createElement('div');
                        screen.className = 'screen';

                        // append the div to the page - one redraw of the DOM tree
                        var elScreens = $element[0].firstChild; //angular.element(document.getElementById('screens'));
                        var renderDiv = elScreens.firstChild; //angular.element(document.getElementById('render'))[0];

                        var margin = 20;

                        // loop through each screen in the dump .. as of 04/15 all dumps have only one screen .. should replace to use [0]

                        for (var iterateScopeDump = 0; iterateScopeDump < $scope.dump.length; iterateScopeDump++) {

                            var shot = $scope.dump[iterateScopeDump];

                            shot = prepElementInfo(shot); // polish the properties set, platform wise.

                            var elementProperties = cleanElementInfo(angular.copy(shot));
                            $scope.list.push(elementProperties);

                            // duplicate - remove when stable

                            // var copy = JSON.parse(JSON.stringify(shot));
                            // copy = cleanElementInfo(copy);
                            // setEnumeration(copy, "tabbedName", $scope.tabString, 0);
                            // $scope.list.push(copy);

                            // render the view
                            var elScreen = document.createElement('div');

                            elScreen.id = shot.hashID;
                            elScreen.style.width = Math.ceil(shot.rect.width * $scope.downScale) + 'px';
                            elScreen.style.height = Math.ceil(shot.rect.height * $scope.downScale) + 'px';

                            // Update parent div
                            var rWidth = Math.ceil((Number(shot.rect.width) + margin) * $scope.downScale);
                            var rHeight = Math.ceil((Number(shot.rect.height) + margin) * $scope.downScale);
                            renderDiv.style.width = rWidth + 'px';
                            renderDiv.style.height = rHeight + 'px';

                            configureViewStyle(elScreen, shot);
                            elScreen.className = 'rootview view';

                            // Base view's superview contains other useful
                            // info, such as orientation, device-screen scale,
                            // device type, etc.
                            var screenInfo = ( shot.superview ) ? shot.superview : shot;
                            if (screenInfo.orientation) {
                                screen.style.width = Math.ceil(shot.rect.width * $scope.downScale) + 'px';
                                screen.style.height = Math.ceil(shot.rect.height * $scope.downScale) + 'px';
                                $scope.lastOrientation = screenInfo.orientation;
                                $scope.deviceScale = ( screenInfo.screen ) ? screenInfo.screen.scale : "1";
                            }

                            screen.appendChild(elScreen);

                            // render sub views
                            // $scope.tabString += $scope.tabCharacter;
                            $scope.tabLevel++;
                            elScreen.style.zIndex = String($scope.tabLevel * 10);

                            for (var iterateSubViews = 0; iterateSubViews < shot.subviews.length; iterateSubViews++) {

                                if ($scope.platform == "android" && shot.class.match("android.widget.ListView") && iterateSubViews >= 1) {

                                    //build divider
                                    //add subview
                                    var divider = buildDivider(shot, shot.subviews[iterateSubViews])
                                    elScreen = addSubView(elScreen, divider)
                                }

                                var subview = shot.subviews[iterateSubViews];

                                subview = prepElementInfo(subview);
                                elScreen = addSubView(elScreen, subview);
                            }

                            // $scope.tabString = $scope.tabString.substring(0, $scope.tabString.length - 3);
                            $scope.tabLevel--;
                        }

                        var ngElScreens = angular.element(elScreens);
                        ngElScreens.empty();

                        // screen.style.width = '950px';
                        // $animate.enter(screen, ngElScreens);

                        var animatePromise = $animate.enter(screen, ngElScreens);

                        animatePromise.then(function() {

                            // called when $animate is completed

                            $scope.width = screen.style.width;
                            $scope.renderComplete = true;

                            refreshAfterChange('view');
                        });

                        $scope.renderElement = angular.element(ngElScreens[0]).children()[0]; // saving for later use

                        if ($scope.lockSelectedFromList) {

                            angular.element($scope.renderElement).addClass('overlay');
                        }

                        $compile($scope.renderElement)($scope);

                        if ($scope.selected) {

                            $scope.selected = document.getElementById($scope.selected.id); // replace selected object with the newly generated element
                            $scope.selectedHistory = null;
                        }
                    }

                    /*
                    *
                    * !refreshAfterChange(whatChanged)
                    *
                    * updated interactivity / overlay / selected overlay after changes
                    *
                    */
                    function refreshAfterChange(whatChanged) {

                        switch(whatChanged) {

                            case 'view': // switching between test view and variation view

                                // if a render is currently in progress, return

                                if (!$scope.renderComplete) return;

                                // change interactivity

                                if ($scope.zoomed) {

                                    addRenderInteractivity();
                                    addRenderSelections();
                                    // addRenderOverlays();

                                } else {

                                    removeRenderInteractivity();
                                    removeRenderSelections();
                                    // removeRenderOverlays();
                                }

                                break;

                            case 'show': // switching between elements by hovering to see their size

                                if ($scope.showHistory) {

                                    angular.element($scope.showHistory).removeClass('show');
                                }

                                if ($scope.show) {

                                    angular.element($scope.show).addClass('show');
                                }

                                break;

                            case 'selected': // switching between elements by clicking to see their properties

                                if ($scope.zoomed) {

                                    if ($scope.selectedHistory) {

                                        angular.element($scope.selectedHistory).removeClass('selected');
                                        $scope.selectedProperties = null;
                                    }

                                    if ($scope.selected) {

                                        // console.log('scope.selected exists' + $scope.selected);

                                        angular.element($scope.selected).addClass('selected');
                                        $scope.selectedProperties = angular.copy($scope.hashMap[$scope.selected.id]);

                                        if($scope.abt) angular.element($scope.renderElement).addClass('overlay');
                                        // console.log("Added overlay");

                                        generatePropertyItems();
                                    }

                                } else {

                                    angular.element($scope.renderElement).removeClass('overlay');

                                    if ($scope.selected) {

                                        angular.element($scope.selected).removeClass('selected');
                                        setDragability(angular.element($scope.selected), false);
                                    }

                                }

                                break;
                        }

                        if (!$scope.$$phase && !$rootScope.$root.$$phase) {

                            $scope.$apply();
                        }
                    }

                    /* ! Render - Interactivity */
                    // Filter for elements which are not being edited
                    $scope.onElementDragStart = function (event, ui) {

                        var rectProperty = null;

                        if ( $scope.platform == 'ios' ) {

                            for ( var i in $scope.properties ) {
                                if ( $scope.properties[i].type == 'rect' ) {
                                    rectProperty = $scope.properties[i];
                                    break;
                                }
                            }

                            if ( rectProperty ) {

                                // if not editing selected property
                                // if clicked element is not selected element
                                if ( rectProperty.editing == false || rectProperty.hashID != event.target.id) {
                                    event.preventDefault();
                                }
                            }

                        // No known changes for android yet, but keeping them
                        // separate in case.
                        } else if ( $scope.platform == 'android' ) {

                            for ( var i in $scope.properties ) {
                                if ( $scope.properties[i].type == 'rect' ) {
                                    rectProperty = $scope.properties[i];
                                    break;
                                }
                            }

                            if ( rectProperty ) {

                                // if not editing selected property
                                // if clicked element is not selected element
                                if ( rectProperty.editing == false || rectProperty.hashID != event.target.id) {
                                    event.preventDefault();
                                }
                            }
                        }
                    }

                    $scope.onElementDrag = function (event, ui) {

                        // TODO : Ensure handles resizing properly as well.

                        if ($scope.selected && $scope.properties) {

                            var rectProperty = null;

                            if ( $scope.platform == 'ios' ) {

                                for ( var i in $scope.properties ) {
                                    if ( $scope.properties[i].type == 'rect' ) {
                                        rectProperty = $scope.properties[i];
                                        break;
                                    }
                                }

                                rectProperty.value = {
                                    x: ui.position.left / $scope.downScale,
                                    y: ui.position.top / $scope.downScale,
                                    width: rectProperty.value.width,
                                    height: rectProperty.value.height
                                }

                                // console.log(JSON.stringify(rectProperty.value, null, 4));

                                if (!$scope.$$phase && !$rootScope.$root.$$phase) {

                                    $scope.$apply();
                                }

                            // No known differences yet, but keeping separate in case...
                            } else if ( $scope.platform == 'android' ) {

                                for ( var i in $scope.properties ) {
                                    if ( $scope.properties[i].type == 'rect' ) {
                                        rectProperty = $scope.properties[i];
                                        break;
                                    }
                                }

                                rectProperty.value = {
                                    x: ui.position.left / $scope.downScale,
                                    y: ui.position.top / $scope.downScale,
                                    width: rectProperty.value.width,
                                    height: rectProperty.value.height
                                }

                                // console.log(JSON.stringify(rectProperty.value, null, 4));

                                if (!$scope.$$phase && !$rootScope.$root.$$phase) {

                                    $scope.$apply();
                                }

                            }

                        }
                    }

                    function setDragability (elSubview, isDraggable) {

                        var el = angular.element(elSubview);
                        if (!el) return;

                        el.attr('jqyoui-draggable', "{animate:true, onDrag : 'onElementDrag', onStart : 'onElementDragStart' }");
                        el.attr('data-jqyoui-options', "{cancel:false}");

                        if ( isDraggable == true ) {
                            el.attr('data-drag', 'true');
                        } else {
                            el.attr('data-drag', 'false');
                        }
                    }

                    function setResizability (elSubview, isResizable) {

                        var el = angular.element(elSubview);
                        if (!el) return;

                        if ( isResizable == true ) {
                            el.resizable({
                                animate: true,
                                handles: "n, e, s, w",
                                disabled : false

                            });
                        } else {
                            var disabled = el.resizable( "option", "disabled" );
                            if (disabled == false)
                                el.resizable( "option", "disabled", true );

                        }
                    }

                    function addRenderInteractivity() {

                        updateRenderInteractivity(true);
                    }

                    function removeRenderInteractivity() {

                        updateRenderInteractivity(false);
                    }

                    function updateRenderInteractivity(status) {

                        // console.log('Updating Interactivity for :');
                        // console.dir($scope.renderElement);

                        var el = $scope.renderElement;

                        if (status == true) {
                            angular.element(el).click(handleClickOnRender);
                            // el.onclick = handleClickOnRender;

                            angular.element(el).mouseover(handleMouseOverOnRender);
                            // el.onmouseover = handleMouseOverOnRender;

                            angular.element(el).mouseout(handleMouseOutOnRender);
                            // el.onmouseout = handleMouseOutOnRender;

                        } else {

                            angular.element(el).click(null);
                            // el.onclick = handleClickOnRender;

                            angular.element(el).mouseover(null);
                            // el.onmouseover = handleMouseOverOnRender;

                            angular.element(el).mouseout(null);
                            // el.onmouseout = handleMouseOutOnRender;

                            // el.onclick = null;
                            // el.onmouseover = null;
                            // el.onmouseout = null;

                        }
                    }

                    /* ! Render - Mouse events */
                    function handleClickOnRender(e) {

                        // console.log('Click Registered: ' + Date.now());

                        // Prevent moving to a new element
                        // while mid editing
                        if ( $scope.editInProgress ) {
                            if (debug) console.log("Edit already in progress...");
                            return;
                        }

                        console.log('Click registered on Render');
                        // console.dir(e);

                        var target = e.target.nodeName == 'IMG' ? e.target.parentNode: e.target;
                        var targetID = target.id;

                        var foundValidTargetOnPopogation = false;

                        while (target.className.indexOf('rootview') < 0) {

                            if (isInteractionAllowedForElement(targetID)) {

                                foundValidTargetOnPopogation = true;
                                break;
                            }

                            target = target.parentNode;
                            targetID = target.id;
                        }

                        if (!foundValidTargetOnPopogation) return;

                        // console.log('choosing ' + targetID);

                        $scope.selectedHistory = $scope.selected;
                        $scope.selected = target;

                        refreshAfterChange('selected');
                    }

                    function handleMouseOverOnRender(e) {

                        // console.log('Click Registered: ' + Date.now());

                        // console.log('Click registered on Render');
                        // console.dir(e);
                        // Prevent moving to a new element
                        // while mid editing
                        if ( $scope.editInProgress ) {
                            return;
                        }

                        var target = e.target.nodeName == 'IMG' ? e.target.parentNode: e.target;
                        var targetID = target.id;

                        var foundValidTargetOnPopogation = false;

                        while (target.className && target.className.indexOf('rootview') < 0) {

                            if (isInteractionAllowedForElement(targetID)) {


                                foundValidTargetOnPopogation = true;
                                break;
                            }

                            target = target.parentNode;
                            targetID = target.id;
                        }

                        if (!foundValidTargetOnPopogation) return;

                        // console.log('choosing ' + targetID);

                        $scope.showHistory = null; // what was being shown earlier, if any
                        $scope.show = target;
                        // console.log(e);
                        refreshAfterChange('show');
                    }

                    function handleMouseOutOnRender(e) {

                        // console.log('MouseOut registered on Render');
                        // console.dir(e);

                        // Prevent moving to a new element
                        // while mid editing
                        if ( $scope.editInProgress ) {
                            return;
                        }

                        var target = e.target.nodeName == 'IMG' ? e.target.parentNode: e.target;
                        var targetID = target.id;

                        var foundValidTargetOnPopogation = false;

                        while (target.className && target.className.indexOf('rootview') < 0) {

                            if (isInteractionAllowedForElement(targetID)) {

                                foundValidTargetOnPopogation = true;
                                break;
                            }

                            target = target.parentNode;
                            targetID = target.id;
                        }

                        if (!foundValidTargetOnPopogation) return;

                        // console.log('choosing ' + targetID);

                        $scope.showHistory = $scope.show;
                        $scope.show = null;

                        refreshAfterChange('show');
                    }

                    /* ! Render - Highlighting Functions */
                    function addRenderSelections() {

                        updateRenderSelections(true);
                    }

                    function removeRenderSelections() {

                        updateRenderSelections(false);
                    }

                    function updateRenderSelections(status) {

                        if (status == true) {


                        }
                    }

                    function highlight(element) {

                        if (!element || $scope.lockSelectedFromList) return;
                        element.style.boxShadow = "inset 0 0 0 2px #f0532d";
                    }

                    function unHighlight(element, force) {

                        if (!force && (!element || $scope.lockSelectedFromList)) return;
                        element.style.boxShadow = "";
                    }

                    /* ! Render - Utility Functions */
                    function updateRenderOverlays() {

                        // remove hightlights

                        // remove overlays

                        console.log($scope.select);

                        if ($scope.select.element) {

                            // highlight the element that is selected

                            // overlay all other elements
                        }
                    }

                    function updatePropertiesOverlays() {

                        // remove overlays

                        if ($scope.select.property) {

                            // overlay everything

                            // remove overlay on selected property
                        }
                    }

                    /*
                    *
                    * clearSelection()
                    *
                    * function to clear the selected element
                    * used only for a/b testing
                    *
                    */
                    function clearSelection() {

                        // return if nothing was selected already

                        if (!$scope.selected) return;

                        // remove lock

                        unHighlight(document.getElementById($scope.selected.hashID));

                        $scope.selectedHistory = $scope.selected;
                        $scope.selected = null;
                        $scope.selectedProperties = null;

                        refreshAfterChange('selected');

                        $scope.lockSelectedFromList = false; // move it up?

                        angular.element($scope.renderElement).removeClass('overlay');
                    }

                    function setEnumeration(obj, key, value, onOff) {

                        if (!obj) return;

                        Object.defineProperty(obj, key, {
                            enumerable: onOff,
                            value: value
                        });
                    }

                    /*
                    *
                    * ! generatePropertyItems()
                    * RENAMED from configureSelected()
                    *
                    * generate properties items from selected / hashmap to be shown in the list
                    *
                    */
                    function generatePropertyItems() {

                        if (!$scope.selectedProperties) return;

                        $scope.properties = [];
                        $scope.selectedProperties.advanced = $scope.selectedProperties.advanced || undefined;

                        for (var key in $scope.selectedProperties) {

                            var item = null; // property item to be inserted into the list

                            var imgData = undefined;
                            var value = $scope.selectedProperties[key];

                            // if mod props knows what to do for the key (?)

                            if (Constants.MOD_PROPS[$scope.platform][key]) {

                                item = angular.copy(Constants.MOD_PROPS[$scope.platform][key]);

                                // iOS type images

                                if ($scope.platform == 'ios' && key == 'image' && $scope.selectedProperties[key]) {

                                    imgData = $scope.selectedProperties[key].imageData; // will be undefined or imageData

                                } else if ($scope.platform == 'android' && $scope.selectedProperties[key]
                                    && (key == 'src' || key == 'background' || key.indexOf('drawable') == 0)) { // Android src/background image or color

                                    // Check by value which type we have
                                    // and set type on the PROPERTY

                                    if ($scope.selectedProperties[key].imageData) {

                                        imgData = $scope.selectedProperties[key].imageData;
                                        item.type = 'image';

                                    } else if ($scope.selectedProperties[key].a !== undefined) {

                                        item.type = 'color';
                                    }
                                }

                                item.value = (imgData) ? imgData : value;
                                item.controller = $scope.selectedProperties.viewController || getDefaultController();
                                item.event = getEvent();
                                item.hashID = $scope.selectedProperties.hashID;
                                item.xpath = $scope.selectedProperties.xpath;

                                if (key === 'advanced' && $scope.selectedProperties.advanced) {

                                    item.value = $scope.selectedProperties.advanced.value;

                                    item.propName = $scope.selectedProperties.advanced.propName; // STODO: set these properties from advanced object
                                    item.propType = $scope.selectedProperties.advanced.propType; // STODO: set these properties from advanced object
                                    item.cellStatus = $scope.selectedProperties.advanced.cellStatus; // STODO: set these properties from advanced object
                                    item.qualifiers = $scope.selectedProperties.advanced.qualifiers; // STODO: set these properties from advanced objectc
                                }

                                if (key === 'rect') {

                                    item.deviceScale = $scope.deviceScale;
                                }

                                if (item.value == undefined && key != 'advanced' ) { // ?

                                    setEnumeration($scope.selectedProperties, key, value, 0);

                                } else {

                                    $scope.properties.push(angular.copy(item));
                                }

                            } else { // if the key isn't defined in MOD_PROPS, set enumerable to false (?)

                                setEnumeration($scope.selectedProperties, key, value, 0);
                            }
                        }
                    }

                    function getDefaultController () {

                        // Extract view controller from root element
                        // if not listed on this particular one
                        return ABConnectService.getAllDumpItems().controller ||
                            ( $scope.dump && $scope.dump[0] ) ? $scope.dump[0].viewController : 'unknown';
                    }

                    function getEvent () {

                        // Trigger point should be viewWillAppear
                        // even if the dump point was viewDidAppear
                        if ( $scope.selectedProperties.event ) {
                            if ( $scope.selectedProperties.event.indexOf('viewDidAppear') > -1 )
                                return 'viewWillAppear:';
                        }

                        // Return a default event based on platform
                        // if none is found in the dump / property list
                        if ( $scope.platform == 'ios' ) {
                            if ( $scope.selectedProperties.cellStatus == 1 ) {
                                return 'tableView:cellForRowAtIndexPath:';
                            } else
                                return 'viewWillAppear:'
                        } else if ($scope.platform == 'android') {
                            return $scope.selectedProperties.event;
                        }
                    }

                    /*  ! Render - Disabled elements */
                    function isInteractionAllowedForElement(id) {

                      // console.log('Checking if ' + id + ',' +$scope.hashMap[id].class + ' is interactable');

                        if (!id || !$scope.hashMap[id]) return false;

                        var unselectableClasses = [
                            "UITableViewCellContentView",
                            "UITableViewWrapperView",
                            "UIViewControllerWrapperView",
                            "UINavigationTransitionView",
                            "UILayoutContainerView",
                            "UITransitionView",
                            'android.widget.CheckBox',
                            'android.widget.RadioButton'
                        ];

                        var unselectableAttributes = [
                            'drawableBottom',
                            'drawableLeft',
                            'drawableRight',
                            'drawableTop'
                        ];

                        return !($scope.hashMap[id].class.indexOf("_") === 0 || unselectableClasses.indexOf($scope.hashMap[id].class) > -1 || Number($scope.hashMap[id].cellStatus) > 1
                            || id.indexOf('NO_ID') == 0 || unselectableAttributes.indexOf($scope.hashMap[id]) > -1);
                    }

                },
                template: ''
                    + '<div class="render">'
                    + '<img src="http://placehold.it/750x1335" class="ab-screen-placeholder" ng-hide="renderComplete">'
                    + '</div>'
            }
        })

        /* !Single Channel RGBA color picker */
        .directive('ensColorPickerChannel', function(Utils) {

            return {

                scope: {
                    "class": "@ensColorChannel",
                    "channelVal": "=ensColorValue",
                    "showLabels" : "="
                },
                controller: function($scope, $element) {

                    $scope.arrowURL = Utils.qualifyURL('/images/tinyArrow.png');

                    $scope.drags = {

                        lastMove: [0, 0],

                        dragStart: function(e) {
                            $scope.width = String(window.getComputedStyle($element[0]).width).replace(/[a-z]/ig, "");
                            $scope.width = Number($scope.width);
                            $scope.widthRatio = 255 / $scope.width;
                            e.stopPropagation();
                            // console.log("on drag start");
                        },

                        dragSlider: function(e) {

                            var parentX = Number(getPosition(e.target.parentNode).x);
                            var mouseX = Number(e.pageX - parentX);


                            // console.log("clentX[%s] vs pos.x[%s]", e.pageX-parentX, parentX);

                            if ( mouseX+1 > $scope.width || mouseX-1 < 0) return;
                            // console.log("Width is : %s. Ratio for slider : %s. clientX[%s / %s]", $scope.width, $scope.widthRatio, mouseX, $scope.width );

                            e.target.style.left = mouseX + "px";

                            if ( $scope.channelVal.isAlpha ) {
                                var alphaVal = String(mouseX / $scope.width).substring(0,4);
                                $scope.channelVal.value = Number(alphaVal);
                                $scope.drags.lastMove = [$scope.drags.lastMove.pop(), Number(alphaVal)];
                            }
                            else {
                                var newVal = Math.round(mouseX * $scope.widthRatio);
                                $scope.channelVal.value = newVal;
                                $scope.drags.lastMove = [$scope.drags.lastMove.pop(), mouseX];
                            }

                            $scope.$apply();
                        },
                        dragEnd: function(e) {
                            if ( $scope.channelVal.isAlpha )
                                e.target.style.left = (Number($scope.drags.lastMove[0]) * $scope.width) + "px";
                            else
                                e.target.style.left = $scope.drags.lastMove[0] + "px";
                            // $scope.channelVal.value = $scope.drags.lastMove[0];
                            $scope.$apply();
                        },
                        clicked : function (e) {
                            var img = new Image();
                            img.src = "./blank.png";
                            img.width = "1px";
                            img.height = "1px";
                            e.dataTransfer.setDragImage(img, 0, 0);
                        }

                    };

                    function getPosition(element) {
                        var xPosition = 0;
                        var yPosition = 0;

                        while(element) {
                            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
                            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
                            element = element.offsetParent;
                        }
                        return { x: xPosition, y: yPosition };
                    }

                    $scope.sliderIcon = $element[0].children[0].children[1];
                    $scope.sliderIcon.ondragstart = $scope.drags.dragStart;
                    $scope.sliderIcon.ondragend = $scope.drags.dragEnd;
                    $scope.sliderIcon.ondrag = $scope.drags.dragSlider;

                    // Set initial values
                    $scope.sliderIcon.style.left = $scope.channelVal + "px";

                },
                template: ''
                    + '<div class="single-color-parent"> <span ng-if="showLabels == true">{{channelVal.channel}} : {{channelVal.value}}</span>'
                    +    '<div class="slider {{class}}"></div>'
                    +    '<img class="icon-alpha" ng-src="{{arrowURL}}">'
                    + '</div>'
            }
        })

        /* !Four Channel RGBA color picker */
        .directive('ensColorPicker', function() {
            return {
                scope : {
                    "showResult" : "=?",
                    "showLabels" : "=?",
                    "result" : "=?ngModel"
                },
                require : "ensColorPickerChannel",
                controller : function($scope, $element) {

                    // console.log("ensColorPicker. Result object : " + $scope.result);
                    $scope.rgba = "";
                    $scope.inverseRgba = "";
                    $scope.sliders = {
                        r: {
                            class: "red-slider",
                            channel: "red",
                            value: $scope.result.r,
                            isAlpha: false
                        },
                        g: {
                            class: "green-slider",
                            channel: "green",
                            value: $scope.result.r,
                            isAlpha: false
                        },
                        b: {
                            class: "blue-slider",
                            channel: "blue",
                            value: $scope.result.r,
                            isAlpha: false
                        },
                        a: {
                            class: "alpha-slider",
                            channel: "alpha",
                            value: $scope.result.r,
                            isAlpha: true
                        }
                    };

                    $scope.sliders.r.value = $scope.result.r;
                    $scope.sliders.g.value = $scope.result.g;
                    $scope.sliders.b.value = $scope.result.b;
                    $scope.sliders.a.value = $scope.result.a;


                    $scope.$watch ( "sliders", function (newVal, oldVal) {

                       // $scope.rgba = ""
                       //     + "rgba("
                       //     + newVal.r.value.toString() + ","
                       //     + newVal.g.value.toString() + ","
                       //     + newVal.b.value.toString() + ","
                       //     + newVal.a.value.toString() + ")";
                        $scope.width = window.getComputedStyle($element[0]).width;
            // console.log("Width is : " + $scope.width);
                        $scope.result = {
                            r: newVal.r.value,
                            g: newVal.g.value,
                            b: newVal.b.value,
                            a: newVal.a.value
                        }

                        $scope.inverseRgba = ""
                            + "rgba("
                            + Number(255 - newVal.r.value).toString() + ","
                            + Number(255 - newVal.g.value).toString() + ","
                            + Number(255 - newVal.b.value).toString() + ","
                            + "1)";

                    }, true );
                },
                template : ''
                    + '<div ng-if="showResult == true" style="width : 25px; height : 25px; background-color : {{rgba}}"></div>'
                    + '<div class="color-picker-container" ng-repeat="slider in sliders">'
                    +   '<div ens-color-picker-channel ens-color-channel="{{slider.class}}" ens-color-value="slider" show-labels="showLabels"></div>'
                    + '</div>'
            };
        })

        /*
        *
        * ! ens-show-property
        *
        * <div ens-show-property="item" ens-on-edit="onEdit" ens-id="id23"> </div>
        *
        * @attr show-property: property item object with .name, .type, .value
        *
        * @attr on-edit : function to call when a property is updated,
        *                 also boolean for allowEdit?
        *
        * @attr id : parameter to send to on-edit to identify
        *
        * displays an element property as fit. text as text. color as div/bg etc.
        * TODO: if ens-on-edit is provided, also adds a click to update function
        *
        */
        .directive('ensShowProperty', function () {
            return {
                restrict: 'A',
                scope: {
                    item: '=ensShowProperty',
                    index: '@ensId',
                    onEdit: '=ensOnEdit'
                },
                controller : function ($scope, $window, $element) {

                    var debug = false;

                    $scope.colorJustChanged = false;

                    $scope.selectText = function($event) {
                        $event.target.select();
                    };

                    $scope.showColorPicker = function(color) {

                        $scope.colorJustChanged = false;

                        if (debug) console.log('showColor()');
                        if (debug) console.dir(color);

                        $scope.onEdit($scope.index);
                    }

                    $scope.hideColorPicker = function(color) {

                        if (debug) console.log('showColor()');
                        if (debug) console.dir(color);

                        if ($scope.colorJustChanged) return;

                        $scope.onEdit($scope.index, true);
                    }

                    $scope.changeColor = function(color) {

                        $scope.colorJustChanged = true;

                        if (debug) console.log('changeColor()')
                        if (debug) console.dir(color);

                        var tc = tinyColorToObj(tinycolor(color)); //.toRgb();
                        $scope.item.value = tc;

                        $scope.onEdit($scope.index);

                    }
                    function tinyColorToObj (tinyColorObject) {
                        return {
                            r : tinyColorObject._r,
                            g : tinyColorObject._g,
                            b : tinyColorObject._b,
                            a : tinyColorObject._a
                        }
                    }

                },
                template: ''

                    // image - view / edit

                    + '<img ng-if="item.type==\'image\'" ng-src="{{item.value}}">'

                    // color - view / edit

                    + '<spectrum-colorpicker ng-model="item.value" ng-if="item.type==\'color\'" on-change="changeColor(color)" on-show="showColorPicker(color)" on-hide="hideColorPicker(color)"'
                        +'options="{disabled: !onEdit, replacerClassName: !onEdit ? \'sp-no-dd\' : \'\', color: JSON.parse(valueString), showInput: true, showAlpha: true, showPalette: true, showSelectionPalette: true, hideAfterPaletteSelect:true, preferredFormat: \'rgb\','
                        +'palette: [[\'#000\',\'#444\',\'#666\',\'#999\',\'#ccc\',\'#eee\',\'#f3f3f3\',\'#fff\'],[\'#f00\',\'#f90\',\'#ff0\',\'#0f0\',\'#0ff\',\'#00f\',\'#90f\',\'#f0f\'],[\'#f4cccc\',\'#fce5cd\',\'#fff2cc\',\'#d9ead3\',\'#d0e0e3\',\'#cfe2f3\',\'#d9d2e9\',\'#ead1dc\'],[\'#ea9999\',\'#f9cb9c\',\'#ffe599\',\'#b6d7a8\',\'#a2c4c9\',\'#9fc5e8\',\'#b4a7d6\',\'#d5a6bd\'],[\'#e06666\',\'#f6b26b\',\'#ffd966\',\'#93c47d\',\'#76a5af\',\'#6fa8dc\',\'#8e7cc3\',\'#c27ba0\'],[\'#c00\',\'#e69138\',\'#f1c232\',\'#6aa84f\',\'#45818e\',\'#3d85c6\',\'#674ea7\',\'#a64d79\'],[\'#900\',\'#b45f06\',\'#bf9000\',\'#38761d\',\'#134f5c\',\'#0b5394\',\'#351c75\',\'#741b47\'],[\'#600\',\'#783f04\',\'#7f6000\',\'#274e13\',\'#0c343d\',\'#073763\',\'#20124d\',\'#4c1130\']]}"> </spectrum-colorpicker>'

                    // advanced - view

                    // + '<div ng-if="(!onEdit || !item.editing) && item.type==\'advanced\'">'
                    //     +'{{item.propName}}</div>'
                    // + '<div ng-if="(!onEdit || !item.editing) && item.type==\'advanced\'">'
                    //     +'{{item.propType}}</div>'
                    // + '<span ng-if="(!onEdit || !item.editing) && item.type==\'advanced\'">'
                    //     +'{{item.value}}</span>'

                    // advanced - edit

                    + '<div class="advanced" ng-if="!!onEdit && item.editing && item.type==\'advanced\'">'

                    + '<input '
                        + 'ng-model="item.propName" placeholder="Name" ng-click="selectText($event)" />'
                    + '<input '
                        + 'ng-model="item.propType" placeholder="Type" ng-click="selectText($event)" />'
                    + '<input '
                        + 'ng-model="item.value" placeholder="Value" ng-click="selectText($event)" />'

                    + '</div>'

                    // rect - view
                    + '<div ng-if="item.type==\'rect\'" ens-rect item="item" ></div>'

                    // default - view

                    + '<span ng-if="(!onEdit || !item.editing) && item.type!=\'color\' && item.type!=\'image\' && item.type!=\'rect\'"> {{item.value | ensLimitTo:item.type!=\'image\':15}} </span>'

                    // default - edit

                    + '<input ng-if="!!onEdit && item.editing && item.type!=\'color\' && item.type!=\'advanced\' && item.type!=\'rect\'" ng-model="item.value" ng-change="printQualifiers()" ng-click="selectText($event)" />'
            };
        })

        .directive("ensRect", function () {
        return {
            restrict : "A",
            scope : {
                item : "=item"
            },
            controller : function ($scope) {
                $scope.toPrecision = function (s, d) {
                    return Math.trimFloatTo(Number(s), 2);
                }
            },
            template : ''
                + '<div class="frame-container">'
                +     '<div class="frame-background">'
                +         '<img class="frame-bgImage" src="http://placenamehere.com/Mozilla/checker.png">'
                +           '<div class="frame-box">'

                +               '<div class="frame-value frame-x" >{{ toPrecision(item.value.x, 2) }}</div>'
                +               '<div class="frame-value frame-y" >{{ toPrecision(item.value.y, 2) }}</div>'
                +               '<div class="frame-value frame-width" >{{ toPrecision(item.value.width, 2) }}</div>'
                +               '<div class="frame-value frame-height" >{{ toPrecision(item.value.height, 2) }}</div>'
                +           '</div>'
                +         '</img>'
                +     '</div>'
                +  '</div>'
            }
        })

        .directive('ngSize', function(){
            return {
                restrict: 'A',
                link: function(scope,element,attrs){
                    if(!element.nodeName === 'SELECT'){
                        return;
                    }
                    attrs.$observe('ngSize', function setSize(value){
                        attrs.$set('size', attrs.ngSize);
                    });
                }
            }
        })

        .filter('ensLimitTo', function() {

            return function (value, force, max, tail, wordwise) {
                if (!value) return '';
                if (force) return value;

                max = parseInt(max, 10);
                if (!max) return value;
                if (value.length <= max) return value;

                value = value.substr(0, max);
                if (wordwise) {
                    var lastspace = value.lastIndexOf(' ');
                    if (lastspace != -1) {
                        value = value.substr(0, lastspace);
                    }
                }

                return value + (tail || ' …');
            };
        })

        /* DEPRECATED / 02/27/2015 / SRRVNN */

        .directive('ensEditable', function (ModificationFormatter) {
            return {
                restrict: 'A',
                scope: {
                    index: '=ensIndex',
                    onEdit: '=ensOnedit',
                    value: '=ensValue',
                    type: '=ensEditable',
                    propName: '=?ensPropName',
                    propType: '=?ensPropType',
                    editing: '=ensEditing'
                },
                controller : function ($scope, $window, $element) {

                    var debug = false;

                    $scope.$watch ("value", function (newVal, oldVal) {

                        if ( newVal && newVal['r'] && newVal['g'] ) {
                            $scope.colorBox = $scope.colorMe(newVal);
                            // console.log("Is a color! : " + $scope.colorBox);
                            $scope.colorBox = {
                                "background-color" : $scope.colorMe($scope.value)
                            }
                        }

                    }, true);

                    $scope.colorMe = function (val) {

                       // console.log("ensEditable:colorMe() : " + val);
                        if (!val) return val;
                        var res = "rgba(" + ModificationFormatter.formatColor(val) + ")";

                        if ($scope.editing && $scope.type == 'color') {

                            $scope.value = val;
                        }

                        return res;
                    }

                    $scope.colorJustChanged = false;

                    $scope.showColorPicker = function(color) {

                        $scope.colorJustChanged = false;

                        if (debug) console.log('showColor()');
                        if (debug) console.dir(color);

                        $scope.onEdit($scope.index);
                    }

                    $scope.hideColorPicker = function(color) {

                        if (debug) console.log('showColor()');
                        if (debug) console.dir(color);

                        if ($scope.colorJustChanged) return;

                        $scope.onEdit($scope.index, true);
                    }

                    $scope.changeColor = function(color) {

                        $scope.colorJustChanged = true;

                        if (debug) console.log('changeColor()')
                        if (debug) console.dir(color);

                        var tc = tinycolor(color).toRgb();
                        $scope.value = tc;

                        $scope.$apply(function(){
                        });

                        $scope.onEdit($scope.index);

                    }

                    $scope.colorBox = {

                        "background-color" : $scope.colorMe($scope.value)
                    }

                },
                template: ''

                    + '<img class="label-img" height="25px" ng-show="type==\'image\'" ng-src="{{ ( type==\'image\' ) ? value : \'\'}}">'
                    // + '<div class="label-color" ng-show="type==\'color\'"'
                    //     + 'ng-style="{\'background-color\':colorMe(value), \'visibility\':!editing || \'hidden\'}"></div>'
                    + '<div class="label-text" ng-hide="editing || type==\'color\' || type==\'image\'"> {{value}} </div>'
                    + '<div class="label-text cheat" ng-show="!editing && type==\'image\'"> {{value | limitTo: 10}} ... </div>'

                    // + '<input class="ens-prop-input" type="color" ng-show="editing && type==\'color\'"'
                    // + 'ng-model="value"/>'

                    + '<input class="input-img" type="text" ng-show="editing && type==\'image\'"'
                    + 'ng-model="value"/>'

                    + '<input class="input-text" type="text" ng-show="editing && [\'string\',\'float\',\'int\',\'bool\',\'attributedText\', \'advanced\'].indexOf(type) > -1"'+ 'ng-model="value"/>'

                    + '<input class="input-text" type="text" ng-show="editing && [\'advanced\'].indexOf(type) > -1"'
                    + 'ng-model="propName" placeholder="prop name"/>'
                    + '<input class="input-text" type="text" ng-show="editing && [\'advanced\'].indexOf(type) > -1"'
                    + 'ng-model="propType" placeholder="eg text"/>'
                    + '<input class="input-text" type="text" ng-show="editing && [\'advanced\'].indexOf(type) > -1"'
                    + 'ng-model="value" placeholder="New text"/>'

                    + '<spectrum-colorpicker class="" ng-model="value" ng-if="type==\'color\'" on-change="changeColor(color)" on-show="showColorPicker(color)" on-hide="hideColorPicker(color)"'
                        +'options="{color: JSON.parse(valueString), showInput: true, showAlpha: true, showPalette: true, showSelectionPalette: true, hideAfterPaletteSelect:true, preferredFormat: \'rgb\','
                        +'palette: [[\'#000\',\'#444\',\'#666\',\'#999\',\'#ccc\',\'#eee\',\'#f3f3f3\',\'#fff\'],[\'#f00\',\'#f90\',\'#ff0\',\'#0f0\',\'#0ff\',\'#00f\',\'#90f\',\'#f0f\'],[\'#f4cccc\',\'#fce5cd\',\'#fff2cc\',\'#d9ead3\',\'#d0e0e3\',\'#cfe2f3\',\'#d9d2e9\',\'#ead1dc\'],[\'#ea9999\',\'#f9cb9c\',\'#ffe599\',\'#b6d7a8\',\'#a2c4c9\',\'#9fc5e8\',\'#b4a7d6\',\'#d5a6bd\'],[\'#e06666\',\'#f6b26b\',\'#ffd966\',\'#93c47d\',\'#76a5af\',\'#6fa8dc\',\'#8e7cc3\',\'#c27ba0\'],[\'#c00\',\'#e69138\',\'#f1c232\',\'#6aa84f\',\'#45818e\',\'#3d85c6\',\'#674ea7\',\'#a64d79\'],[\'#900\',\'#b45f06\',\'#bf9000\',\'#38761d\',\'#134f5c\',\'#0b5394\',\'#351c75\',\'#741b47\'],[\'#600\',\'#783f04\',\'#7f6000\',\'#274e13\',\'#0c343d\',\'#073763\',\'#20124d\',\'#4c1130\']]}"> </spectrum-colorpicker>'

                    // + '<div class="input-color" ng-if="type==\'color\'"><div class="label-color-cheat" ng-show="editing"'
                    //     +' ng-style="{\'background-color\':colorMe(value)}"></div> <div ng-show="editing" class="input-color-text">{{colorMe(value)}}</div><div ens-color-picker class="ens-prop-input" ng-show="editing && type==\'color\'" ng-model="value"></div></div>'
            };
        })

        /* DEPRECATED / 02/27/2015 / SRRVNN / DWS */

        .directive('ensDisplay', function(ModificationFormatter) {
            return {
                restrict: 'A',
                scope: {
                    value: '=ensValue',
                    type: '=ensDisplay',
                    propName: '=?ensPropName',
                    propType: '=?ensPropType',
                    elLocation: '=?ensElLocation'
                },
                controller : function ($scope, $window, $element) {

                   // console.log("value: %s\ntype: %s\npropName: %s\npropType: %s\nediting: %s\n", $scope.value, $scope.type, $scope.propName, $scope.propType, $scope.editing);

                    $scope.colorMe = function (val) {

                        // console.log("ensEditable:colorMe() : " + val);
                        var res = "rgba(" + ModificationFormatter.formatColor(val) + ")";

                        if ($scope.editing && $scope.type == 'color') {

                            $scope.value = val;
                        }

                        return res;
                    }

                    $scope.colorBox = {

                        "background-color" : $scope.colorMe($scope.value)
                    }
                },
                template: ''

                    + '<img class="label-img" height="25px" ng-show="type==\'image\'" ng-src="{{ ( type==\'image\' ) ? value : \'\'}}">'

                    + '<div class="label-color" ng-show="type==\'color\'"'
                        + 'ng-style="{\'background-color\':colorMe(value)}"></div>'

                    + '<div class="label-text" ng-hide="type==\'color\' || type==\'image\' || type==\'advanced\'"> {{value | limitTo: 25}} </div>'

                    + '<div class="label-text" ng-show="type==\'advanced\'"> {{propName | limitTo: 25}} </div>'
                    + '<div class="label-text" ng-show="type==\'advanced\'"> {{propType | limitTo: 25}} </div>'
                    + '<div class="label-text" ng-show="type==\'advanced\'"> {{value | limitTo: 25}} </div>'




            };
        })

});
