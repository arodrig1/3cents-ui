(function(global, factory) {
   if (typeof define === 'function' && define.amd) {
      define([], factory);
   } else if (typeof module !== 'undefined' && module.exports) {
      module.exports = factory();
   } else {
      global.ReconnectingWebSocket = factory();
   }
})(this, function() {

   var _constants;
   var debug = false;

    if ( !Math.trimFloatTo ) {
        Object.defineProperty(Math, 'trimFloatTo', {
            value: function(num, numDecimals) {

                if ( !num || num == NaN ) return null;

                var s = String(num);
                var splits = s.split(/\./);
                if (!splits || splits.length < 2) return num;

                var fraction = splits[1];
                if ( numDecimals === null || numDecimals === undefined || numDecimals === NaN )
                    return num;

                return Number(splits[0] + "." + fraction.slice(0,numDecimals) );

            },
            enumerable: false // = Default
        });
    }

   _constants = {
      'MOD_PROPS': {

         ios: {
            "class": {
               name: "className",
               type: "disabled",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "alpha": {
               name: "alpha",
               type: "float",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "backgroundColor": {
               name: "backgroundColor",
               type: "color",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "hidden": {
               name: "hidden",
               type: "bool",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "image": {
               name: "image",
               type: "image",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "text": {
               name: "text",
               type: "string",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "attributedText": {
               name: "attributedText",
               type: "attributedText",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "textColor": {
               name: "textColor",
               type: "color",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "tintColor": {
               name: "tintColor",
               type: "color",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            },
            "rect": {
               name: "rect",
               type: "rect",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined,
               deviceScale : undefined

            },
            "advanced": {
               name: "advanced",
               type: "advanced",
               value: '',
               propName: undefined,
               propType: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined

            }
         },

         android: {
            "class": {
               name: "className",
               type: "disabled",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "src": {
               name: "src",
               type: undefined,
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "background": {
               name: "background",
               type: undefined,
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "text": {
               name: "text",
               type: "string",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "textColor": {
               name: "textColor",
               type: "color",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "textOff": {
               name: "textOff",
               type: "string",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "textOn": {
               name: "textOn",
               type: "string",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "thumb": {
               name: "thumb",
               type: undefined,
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "track": {
               name: "track",
               type: undefined,
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "tintColor": {
               name: "tintColor",
               type: "color",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "visibility": {
               name: "visibility",
               type: "int",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },
            "rect": {
               name: "rect",
               type: "rect",
               value: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined,
               deviceScale : undefined
            },
            "advanced": {
               name: "advanced",
               type: "advanced",
               value: '',
               propName: undefined,
               propType: undefined,
               controller: undefined,
               hashID: undefined,
               xpath: undefined,
               cellStatus: undefined,
               editing: false,
               original: false,
               qualifiers: undefined
            },

         }
      },
      'UTILS': {
         'android': {
            "divStylesDrawable": function(el, data, attribute, platform, $scope) {
               // attribute could be -- drawable[Right,Left,Top,Bottom]
               if ( !attribute || !data || !data[attribute])
                   return el;

               if (data[attribute].imageData) {
                   var elSubviewImg = document.createElement('img');

                   elSubviewImg.style.width = Math.ceil(data[attribute].rect.width * $scope.downScale) + 'px';    // calculate el.style.width;
                   elSubviewImg.style.height = Math.ceil(data[attribute].rect.height * $scope.downScale) + 'px';  // calculate el.style.height;

                   // style changes
                   if (attribute == 'drawableBottom' || attribute == 'drawableTop') {
                       elSubviewImg.style.display = 'block';
                       elSubviewImg.style.margin = '0 auto';
                   }
                   else if (attribute == 'drawableLeft')
                       elSubviewImg.style.float = 'left';
                   //else if (attribute == 'drawableRight' || attribute == 'thumb' || attribute == 'track')
                   else if (attribute == 'drawableRight')
                       elSubviewImg.style.float = 'right';

                   if (data[attribute].imageData)
                       elSubviewImg.src = data[attribute].imageData;

                   if (attribute == 'drawableTop')
                       $(el).prepend(elSubviewImg);
                   //else if (attribute == 'drawableLeft' || attribute == 'drawableRight' || attribute == 'thumb' || attribute == 'track') {
                   else if (attribute == 'drawableLeft' || attribute == 'drawableRight') {
                       $(el).find('p').append(elSubviewImg);
                   }
                   else
                       el.appendChild(elSubviewImg);
               }
               else if (data[attribute].a !== undefined) {
                   var elForegroundChild = document.createElement('div');
                   elForegroundChild.id = el.id + "-foreground";
                   elForegroundChild.className = 'view';
                   elForegroundChild.style.width = Math.ceil(data.rect.width * $scope.downScale) + 'px';
                   elForegroundChild.style.height = Math.ceil(data.rect.height * $scope.downScale) + 'px';

                   var color = '' + "rgba(" + data[attribute].r + "," + data[attribute].g + "," + data[attribute].b + "," + data[attribute].a + ")";

                   elForegroundChild.style.backgroundColor = color;

                   el.appendChild(elForegroundChild);
               }

               return el;
            },
            "dumpPrepDrawable": function(data, attribute, platform, $scope) {
               // attribute could be -- drawable[Right,Left,Top,Bottom], thumb, track
               if ( !attribute || !data[attribute]) return undefined;

               if (data[attribute].a !== undefined) {
                   // _constants.MOD_PROPS.android[attribute].type = "color";
                   return data[attribute] || {
                       r: "0",
                       g: "0",
                       b: "0",
                       a: "0"
                   };
               }
               else if (data[attribute].imageData) {
                   // _constants.MOD_PROPS.android[attribute].type = "image";

                   var image = {};

                   var d = data[attribute];
                   var imageType = d['imageType'];
                   // var imageData = new Image(buttonRect.width, buttonRect.height);
                   // imageData[attribute] = "data:" + imageType + ";base64," + data['image'];
                   if (d['imageData'].toString().indexOf("data:") === 0 || d['imageData'].toString().indexOf("http") === 0)
                       image['imageData'] = d['imageData'];
                   else
                       image['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                   image['imageType'] = imageType;
                   image['imageSize'] = (data[attribute].imageSize) ? data[attribute].imageSize : 1;
                   image.rect = {
                       'width' : data[attribute].rect.width,
                       'height' : data[attribute].rect.height
                   };
                   data[attribute] = image;
               }
               else {

                   var url = data[attribute];

                   data[attribute] = {}
                   data[attribute].imageData = url;
               }

               // console.log("Item %s.image : %s", data.hashID, JSON.stringify(data.image));

               return data[attribute];
            }
         }
      },
      'DIV_STYLES': {
         'ios': {
            'style': {
               'height': function(el, data, platform, $scope) {
                  return (data.rect.height * $scope.downScale) + 'px';
               },
               'width': function(el, data, platform, $scope) {
                  // HTML element text truncation issue fix
                  // if (data.text)
                  //    return 'auto';

                  return (data.rect.width * $scope.downScale) + 'px';
               },
               'top': function(el, data, platform, $scope) {

                  if (data.canScroll)
                     return ((data.rect.y - data.contentOffset.y) * $scope.downScale) + 'px';
                  return (data.rect.y * $scope.downScale) + 'px';

               },
               'left': function(el, data, platform, $scope) {

                  if (data.canScroll)
                     return ((data.rect.x - data.contentOffset.x) * $scope.downScale) + 'px';
                  return (data.rect.x * $scope.downScale) + 'px';

               },
               'opacity': function(el, data, platform, $scope) {
                  return data.alpha;
               },
               'backgroundColor': function(el, data, platform, $scope) {
                  var color = '' + "rgba(" + data.backgroundColor.r + "," + data.backgroundColor.g + "," + data.backgroundColor.b + "," + data.backgroundColor.a + ")";

                  return color;
               },
               'color': function(el, data, platform, $scope) {

                  if (!data.text) return '';

                  var color = '' + "rgba(" + data.textColor.r + "," + data.textColor.g + "," + data.textColor.b + "," + data.textColor.a + ")";

                  return color;
               },
               'textAlign': function(el, data, platform, $scope) {

                  if (!data.text) return '';
                  return data.textAlignment;

               },
               'lineHeight': function(el, data, platform, $scope) {

                  if (!data.text) return '';
                  return '';

               },
               'backgroundImage': function(el, data, platform, $scope) {
                  if (!data.backgroundImage) return '';
                  return "url(" + data.backgroundImage.imageData + ")";
               },
               'backgroundSize': function(el, data, platform, $scope) {
                  return (data.rect.width * $scope.downScale) + 'px ' + (data.rect.height * $scope.downScale) + 'px';
               },
               'backgroundRepeat': function(el, data, platform, $scope) {
                  return "no-repeat";
               },
               'fontSize': function(el, data, platform, $scope) {
                  if (!data.text) return '';

                  return (data.font.pointSize * $scope.downScale) + 'px';
               },
               'zIndex': function(el, data, platform, $scope) {
                  return String($scope.tabLevel * 10);
               }
            },
            'innerHTML': function(el, data, platform, $scope) {
               if (data.text)
                  return data.text;
               return '';
            },
            'src': function(el, data, platform, $scope) {

               if (data.image) {

                  var elSubviewImg = document.createElement('img');

                  elSubviewImg.style.width = el.style.width;
                  elSubviewImg.style.height = el.style.height;

                  elSubviewImg.style.left = el.style.left;
                  elSubviewImg.style.top = el.style.top;


                  if (data.image && data.image.imageData) {

                     var imageSource = data.image.imageData;

                     if (data.tintColor) {
                        // console.log(JSON.stringify(subview.tintColor));
                        try {
                           imageSource = data.image.imageData;

                           // imageSource = ImageTinter.tintImage(subview.image.imageData, subview.tintColor, { width : subview.rect.width, height : subview.rect.height });
                        } catch (e) {
                           console.log("Failed to tint image : " + e);
                           imageSource = data.image.imageData;
                        }
                     }

                     elSubviewImg.src = imageSource;
                     // console.log("Image %s.src : %s", subview.hashID, (imageSource.length > 250) ? imageSource.substring(0,250) : imageSource);
                  }


                  el.appendChild(elSubviewImg);

               }

               return el;
            },
            'id': function(el, data, platform, $scope) {
               return data.hashID;
            },
            'className': function(el, data, platform, $scope) {
               return 'view';
            }
         },
         'android': {
            'id': function(el, data, platform, $scope) {
               return data.hashID;
            },
            'innerHTML': function(el, data, platform, $scope) {
               return '';
            },
            'background': function(el, data, platform, $scope) {

               if (data.background && data.background.imageData) {

                  var container = document.createElement("div");
                  var elSubviewImg = document.createElement('img');

                  if (debug) console.log("Setting android background as image for class " + data.class);

                  elSubviewImg.style.width = Math.ceil(data.rect.width * $scope.downScale) + 'px'; //el.style.width;
                  elSubviewImg.style.height = Math.ceil(data.rect.height * $scope.downScale) + 'px'; //el.style.height;

                  elSubviewImg.style.position = 'absolute';
                  elSubviewImg.style.left = "0px"; //el.style.left;
                  elSubviewImg.style.top = "0px"; //el.style.top;

                  if (data.background && data.background.imageData) // redundant. the check happened a few lines above
                     elSubviewImg.src = data.background.imageData;

                  el.style.position = "absolute";
                  el.style.top = "0";
                  el.style.left = "0";
                  el.style.width = "100%";
                  el.style.height = "100%";

                  container.appendChild(elSubviewImg);
                  container.appendChild(el);
                  el = container;
                  // el.appendChild(elSubviewImg);

               } else if (data.background && data.background.a !== undefined) {

                  // var elBackgroundChild = document.createElement('div');
                  // elBackgroundChild.id = el.id + "-background";
                  // elBackgroundChild.className = 'view';
                  // elBackgroundChild.style.width = (data.rect.width * $scope.downScale) + 'px';
                  // elBackgroundChild.style.height = (data.rect.height * $scope.downScale) + 'px';

                  // elBackgroundChild.style.left = "0px";
                  // elBackgroundChild.style.top = "0px";

                  var color = ''
                        + "rgba("
                        + data.background.r + ","
                        + data.background.g + ","
                        + data.background.b + ","
                        + data.background.a + ")";

                  // elBackgroundChild.style.backgroundColor = color;
                  el.style.backgroundColor = color;

                  // el.appendChild(elBackgroundChild);
               }

               return el;
            },
            'text': function(el, data, platform, $scope) {

                if (data.text) {

                    var elSubviewP = document.createElement('p');

                    elSubviewP.innerHTML = data.text;

                    elSubviewP.style.position = 'absolute';
                    elSubviewP.style.left = "0px";
                    elSubviewP.style.top = "0px";
                    elSubviewP.style.width = '100%';
                    elSubviewP.style.height = '100%';
                    elSubviewP.style.margin = '0';

                    if (data.class.indexOf('android.widget') > -1) {

                        elSubviewP.style.display = 'inline-block';
                    }

                    if (data.track) {

                        // set text to be relative to share space with track

                        elSubviewP.style.position = 'relative';

                        // give paragraph a margin right

                        elSubviewP.style['margin-right'] = '15px';

                        // give paragraph only the left over width after (track plus margin for the track)

                        elSubviewP.style.width = Math.ceil((data.rect.width  - data.track.width - 30) * $scope.downScale) + 'px';
                    }

                    el.appendChild(elSubviewP);
                }

            },
            'drawableTop': function(el, data, platform, $scope) {
               return _constants.UTILS.android.divStylesDrawable(el, data, 'drawableTop', platform, $scope);
            },
            'drawableLeft': function(el, data, platform, $scope) {
               return _constants.UTILS.android.divStylesDrawable(el, data, 'drawableLeft', platform, $scope);
            },
            'drawableRight': function(el, data, platform, $scope) {
               return _constants.UTILS.android.divStylesDrawable(el, data, 'drawableRight', platform, $scope);
            },
            'drawableBottom': function(el, data, platform, $scope) {
               return _constants.UTILS.android.divStylesDrawable(el, data, 'drawableBottom', platform, $scope);
            },
            'src': function(el, data, platform, $scope) {
               if (data.src && data.src.imageData) {
                  var elSubviewImg = document.createElement('img');

                  elSubviewImg.style.left = "0px"; //el.style.left;
                  elSubviewImg.style.top = "0px"; //el.style.top;

                  elSubviewImg.src = data.src.imageData;

                  if (debug) console.log("Setting android src as image for class " + data.class);

                  // Use bitmap size or default to parent rect
                  if ( data.src.width ) {
                      elSubviewImg.style.width = Math.ceil(data.src.width * $scope.downScale) + 'px';
                  } else {
                      elSubviewImg.style.width = Math.ceil(data.rect.width * $scope.downScale) + 'px';
                  }

                  if ( data.src.height ) {
                      elSubviewImg.style.height = Math.ceil(data.src.height * $scope.downScale) + 'px';
                  } else {
                      elSubviewImg.style.height = Math.ceil(data.rect.height * $scope.downScale) + 'px';
                  }


                  if (data.class.indexOf('android.widget') > -1 )
                      $(el).prepend(elSubviewImg);
                  else
                      el.appendChild(elSubviewImg);

               }
               else if (data.src && data.src.a !== undefined) {

                  var elForegroundChild = document.createElement('div');
                  elForegroundChild.id = el.id + "-foreground";
                  elForegroundChild.className = 'view';
                  elForegroundChild.style.width = Math.ceil(data.rect.width * $scope.downScale) + 'px';
                  elForegroundChild.style.height = Math.ceil(data.rect.height * $scope.downScale) + 'px';

                  elForegroundChild.style.left = "0px";
                  elForegroundChild.style.top = "0px";

                  var color = '' + "rgba(" + data.src.r + "," + data.src.g + "," + data.src.b + "," + data.src.a + ")";

                  elForegroundChild.style.backgroundColor = color;

                  el.appendChild(elForegroundChild);
               }

               return el;
            },
            'thumb': function(el, data, platform, $scope) {
               //return _constants.UTILS.android.divStylesDrawable(el, data, 'thumb', platform, $scope);
            },
            'track': function(el, data, platform, $scope) {

                if (!data.track || !data.thumb) return; // if data doesn't have track, thumb info, return

                // add an image for the track

                elImgTrack = document.createElement('img');
                elImgTrack.style.position = 'absolute';
                elImgTrack.style.width = Math.ceil(data.track.width * $scope.downScale) + 'px';
                elImgTrack.style.height = Math.ceil(data.track.height * $scope.downScale) + 'px';
                elImgTrack.style.background = '#252928';
                elImgTrack.style['border-radius'] = '2px';
                elImgTrack.style['box-sizing'] = 'border-box';
                elImgTrack.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
                // elImgTrack.src = 'data:image/png;base64,' + data.track.imageData;

                // add an image for the thumb

                elImgThumb = document.createElement('img');
                elImgThumb.style.position = 'absolute';
                elImgThumb.style.width = Math.ceil(data.thumb.width * $scope.downScale) + 'px';
                elImgThumb.style.height = Math.ceil(data.thumb.height * $scope.downScale) + 'px';
                elImgThumb.style.background = '#545454';
                elImgThumb.style.border = '2px solid #252928'
                elImgThumb.style['border-radius'] = '4px';
                elImgThumb.style['box-sizing'] = 'border-box';
                elImgThumb.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
                // elImgThumb.src = 'data:image/png;base64,' + data.thumb.imageData;

                // add a p for the thumb text

                elSpanThumb = document.createElement('span');
                elSpanThumb.innerHTML = data.textOn || data.textOff;

                elSpanThumb.style.position = 'absolute';
                elSpanThumb.style.width = Math.ceil(data.thumb.width * $scope.downScale) + 'px';
                elSpanThumb.style.height = Math.ceil(data.thumb.height * $scope.downScale) + 'px';
                elSpanThumb.style['line-height'] = Math.ceil(data.thumb.height * $scope.downScale) + 'px';
                elSpanThumb.style['text-align'] = 'center';
                elSpanThumb.style.transform = 'scaleY(0.9)';
                elSpanThumb.style['font-weight'] = '200';

                // move the switch to on position

                if (data.textOn) {

                    elImgThumb.style.transform = 'translateX(100%)';
                    elImgThumb.style.background = '#0381a7';
                    elSpanThumb.style.transform += 'translateX(100%)';
                }

                el.appendChild(elImgTrack);
                el.appendChild(elImgThumb);
                el.appendChild(elSpanThumb);

            },
            'className': function(el, data, platform, $scope) {
               return 'view';
            },
            'style': {
               'height': function(el, data, platform, $scope) {
                if (data.text && data.layout_height == _constants.ANDROID.LAYOUTPARAMS.MATCH_PARENT)
                    return '100%';

                  return Math.ceil(data.rect.height * $scope.downScale) + 'px';
               },
               'width': function(el, data, platform, $scope) {
                  if (data.text && data.layout_width == _constants.ANDROID.LAYOUTPARAMS.MATCH_PARENT)
                      return '100%';

                  return Math.ceil(data.rect.width * $scope.downScale) + 'px';
               },
                'whiteSpace' : function(el, data, platform, $scope){
                    return 'pre-line';
                },
               'top': function(el, data, platform, $scope) {

                  var top;
                  // Hack to offset missing root view
                  if ($scope.platform == "android" && data.orientation )
                     top = '0px';
                  else
                     top = (data.rect.y * $scope.downScale) + 'px';
                  return top;

               },
               'left': function(el, data, platform, $scope) {

                  return (data.rect.x * $scope.downScale) + 'px';
               },
               'opacity': function(el, data, platform, $scope) {
                  if (data.visibility == _constants.ANDROID.VIEW.INVISIBLE)
                      return 0.1;
                  return data.alpha;
               },
               'color': function(el, data, platform, $scope) {

                  if (!data.text) return '';

                  var color = '' + "rgba(" + data.textColor.r + "," + data.textColor.g + "," + data.textColor.b + "," + data.textColor.a + ")";

                  return color;
               },
               'textAlign': function(el, data, platform, $scope) {

                  if (!data.text) return '';

                  //defaulting android until it's pulled
                  var ret = 'left';
                  if (data.class.match('android.widget.Button') || data.class.match('android.widget.ToggleButton'))
                     ret = "center";

                  return ret;
               },
               'lineHeight': function(el, data, platform, $scope) {

                  if (!data.text) return '';

                  if (data.class.match('android.widget.Button') || data.class.match('android.widget.ToggleButton'))
                     return el.style.height;

               },
               'fontSize': function(el, data, platform, $scope) {
                  if (!data.text) return '';
                  return (data.font.pixelSize * $scope.downScale) + 'px';

               },
               'fontFamily': function(el, data, platform, $scope) {
                  if (!data.text) return '';
                  return data.font.fontName;
               },
                'zIndex': function(el, data, platform, $scope) {
                    return String($scope.tabLevel * 10);
               }
            }
         }
      },
      'DUMP_PREP': {
         ios: {
            'backgroundColor': function(data, platform, $scope) {
               /* !backgroundColor
                * Default to "clear" color */
               return data.backgroundColor || {
                  r: "0",
                  g: "0",
                  b: "0",
                  a: "0"
               };
            },
            'viewController': function(data, platform, $scope) {
               if (data.viewController && data.viewController.indexOf('null') === -1)
                  $scope.viewControllers[data.viewController] = true;
               return data.viewController;
            },
            'globalRect': function(data, platform, $scope) {

               /* !globalRect
                * Possibly unneeded */

               // TODO : Not needed...?
               if (data.globalRect) {
                  data.scaledRect = {
                     x: data.globalRect.x,
                     y: data.globalRect.y,
                     width: data.globalRect.width,
                     height: data.globalRect.height
                  };
               }

               return data.globalRect;
            },
            'contentOffset': function(data, platform, $scope) {
               /* !contentOffset
                * Support for dumping with scrolled offsets  */

               if (data.contentOffset)
                  data.canScroll = (data.contentOffset) ? true : false;

               return data.contentOffset;
            },
            'image': function(data, platform, $scope) {

               /* !image
                * Format images */

               var image = {};
               if (data['image']) {

                  if (data['image']['imageData']) {

                     var d = data['image'];
                     var imageType = d['imageType'];
                     // var imageData = new Image(buttonRect.width, buttonRect.height);
                     // imageData.src = "data:" + imageType + ";base64," + data['image'];
                     if (d['imageData'].toString().indexOf("data:") === 0 || d['imageData'].toString().indexOf("http") === 0)
                        image['imageData'] = d['imageData'];
                     else
                        image['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                     image['imageType'] = imageType;
                     image['imageSize'] = (data.image.imageSize) ? data.image.imageSize : 1;
                     data.image = image;

                  } else {

                     var url = data.image;

                     data.image = {}
                     data.image.imageData = url;
                  }

                  // console.log("Item %s.image : %s", data.hashID, JSON.stringify(data.image));
               }

               return data.image;
            },
            'backgroundImage': function(data, platform, $scope) {

               /* !backgroundImage
                * Format background images */
               var bgImage = {};
               if (data['backgroundImage']) {

                  if (data['backgroundImage']['imageData']) {

                     var d = data['backgroundImage'];
                     var imageType = d['imageType'];
                     // var imageData = new Image(buttonRect.width, buttonRect.height);
                     // imageData.src = "data:" + imageType + ";base64," + data['image'];
                     if (d['imageData'].toString().indexOf("data:") === 0)
                        bgImage['imageData'] = d['imageData'];
                     else
                        bgImage['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                     bgImage['imageType'] = imageType;
                     data.backgroundImage = bgImage;
                  } else {

                     var url = data.backgroundImage;

                     data.backgroundImage = {}
                     data.backgroundImage.imageData = url;
                  }
               }

               return data.backgroundImage;
            },
            'text': function(data, platform, $scope) {
               if (data.text) {
                  if (data.font.pointSize)
                     data.font.pointSize = Number(data.font.pointSize);
                  data.font.pixelSize = Number(data.font.pixelSize);
               }
               return data.text;
            },
            'hashID': function(data, platform, $scope) {

               if ($scope.hashMap[data.hashID] !== undefined) {
                  console.error("Element (%s)%s already exists! \nOriginal : %s", data.class, data.hashID, $scope.hashMap[data.hashID].class);
               }

               $scope.hashMap[data.hashID] = data;

               return data.hashID;
            }
         },
         android: {
            'viewController': function(data, platform, $scope) {

                // adds the view controller information to view controllers hash in scope

                if (data.viewController && data.viewController.indexOf('null') === -1)
                    $scope.viewControllers[data.viewController] = true;

               return data.viewController;
            },
            'src': function(data, platform, $scope) {

               /* !image
                * Format images */
               if (!data.src) return undefined;

               if (data.src.a !== undefined) {
                  // _constants.MOD_PROPS.android.src.type = "color";
                  return data.src || {
                     r: "0",
                     g: "0",
                     b: "0",
                     a: "0"
                  };
               } else if (data.src.imageData) {

                  // _constants.MOD_PROPS.android.src.type = "image";

                  var image = {};

                  var d = data['src'];
                  var imageType = d['imageType'];
                  // var imageData = new Image(buttonRect.width, buttonRect.height);
                  // imageData.src = "data:" + imageType + ";base64," + data['image'];
                  if (d['imageData'].toString().indexOf("data:") === 0 || d['imageData'].toString().indexOf("http") === 0)
                     image['imageData'] = d['imageData'];
                  else
                     image['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                  image['imageType'] = imageType;
                  image['imageSize'] = (data.src.imageSize) ? data.src.imageSize : 1;
                  if ( d.width ) image['width'] = d.width;
                  if ( d.height ) image['height'] = d.height;

                  data.src = image;

               } else {

                  var url = data.src;

                  data.src = {}
                  data.src.imageData = url;
               }

               // console.log("Item %s.image : %s", data.hashID, JSON.stringify(data.image));

               return data.src;
            },
            'background': function(data, platform, $scope) {

               /* !backgroundImage
                * Format background images */
               if (!data.background) return {
                     r: "0",
                     g: "0",
                     b: "0",
                     a: "0"
               };

               if (data.background.a !== undefined) {
                  // _constants.MOD_PROPS.android.background.type = "color";
                  return data.background || {
                     r: "0",
                     g: "0",
                     b: "0",
                     a: "0"
                  };
               } else if (data.background.imageData) {

                  // _constants.MOD_PROPS.android.background.type = "image";

                  var image = {};

                  var d = data['background'];
                  var imageType = d['imageType'];
                  // var imageData = new Image(buttonRect.width, buttonRect.height);
                  // imageData.src = "data:" + imageType + ";base64," + data['image'];
                  if (d['imageData'].toString().indexOf("data:") === 0 || d['imageData'].toString().indexOf("http") === 0)
                     image['imageData'] = d['imageData'];
                  else
                     image['imageData'] = "data:" + imageType + ";base64," + d['imageData'];

                  image['imageType'] = imageType;
                  image['imageSize'] = (data.background.imageSize) ? data.background.imageSize : 1;
                  data.background = image;

               } else {

                  var url = data.background;

                  data.background = {}
                  data.background.imageData = url;
               }

               // console.log("Item %s.image : %s", data.hashID, JSON.stringify(data.image));

               return data.background;
            },
            'drawableBottom': function(data, platform, $scope) {
               return  _constants.UTILS.android.dumpPrepDrawable(data, 'drawableBottom', platform, $scope);
            },
            'drawableLeft': function(data, platform, $scope) {
               return  _constants.UTILS.android.dumpPrepDrawable(data, 'drawableLeft', platform, $scope);
            },
            'drawableRight': function(data, platform, $scope) {
               return  _constants.UTILS.android.dumpPrepDrawable(data, 'drawableRight', platform, $scope);
            },
            'drawableTop': function(data, platform, $scope) {
               return  _constants.UTILS.android.dumpPrepDrawable(data, 'drawableTop', platform, $scope);
            },
            'text': function(data, platform, $scope) {
                if (data.text) {

                    if (data.font) {
                        if (data.font.pointSize) {
                            data.font.pointSize = Number(data.font.pointSize);
                        } else {
                            data.font.pixelSize = Number(data.font.pixelSize);
                        }

                        // Use Roboto Condensed as default
                        if (!data.font.fontName) data.font.fontName = 'Roboto Condensed, sans-serif';
                    }
                }
                return data.text;
            },
            // 'thumb': function(data, platform, $scope) {
            //    return data.thumb;
            // },
            // 'track': function(data, platform, $scope) {
            //    return data.track;
            // },
            'hashID': function(data, platform, $scope) {
               if ($scope.hashMap[data.hashID] !== undefined) {
                  console.error("Element (%s)%s already exists! \nOriginal : %s", data.class, data.hashID, $scope.hashMap[data.hashID].class);
               } else if (data.hashID === '0xffffffff') {
                  $scope.noId++;
                  data.hashID = String('NO_ID-' + $scope.noId);
               } else if (data.position !== undefined && data.hashID.indexOf('|position-') === -1 ) {
                  data.hashID += '|position-' + data.position;
               }

               $scope.hashMap[data.hashID] = data;

               return data.hashID;
            }
         }
      },
      'ANDROID': {
         'LAYOUTPARAMS': {
             'MATCH_PARENT': -1,
             'WRAP_CONTENT': -2
         },
         'VIEW': {
            'VISIBLE': 0,
            'INVISIBLE': 4,
            'GONE': 8
         }
      },
      'AUTH_ENDPOINT': 'https://api.ensighten.com/flash/login',
      'SOCKET_ENDPOINT': (function() {
         // IIFE to return the right socket address

         if (document.location.hostname != 'localhost') {
            return 'https://' + window.location.host + '/';
         } else {
            return 'http://' + window.location.host + '/';
         }
      })(),
      'SOCKET_REQUESTS': {
         'SCREEN_REQUEST': 'screen.request',
         'CLASS_STRUCT_REQUEST': 'class.struct.request',
         'CLASS_FULL_REQUEST': 'class.fullDump.request',
         'OBJ_STRINGIFY_REQUEST': 'obj.stringify.request',

         'IDENTIFY': 'identify',
         'GET_DEVICES': 'get.device.list',
         'CONTROL_DEVICE': 'device.control',
         'RELEASE_DEVICE': 'device.release'
      },
      'SOCKET_RESPONSES': {
         'SCREEN_RECEIPT': 'screen.receipt',
         'CLASS_STRUCT_RECEIPT': 'class.struct.receipt',
         'CLASS_FULL_RECEIPT': 'class.fullDump.receipt',
         'OBJ_STRINGIFY_REQUEST': 'obj.stringify.receipt',


         'DEVICES': 'device.list',
         'SESSION_CONNECT': 'session.connect',
         'SESSION_DISCONNECT': 'session.disconnect',
         "DEVICE_CONNECT": "device.connect",
         "DEVICE_DISCONNECT": "device.disconnect",
         "MODIFY_ACK": "modify.ack",
         "MODIFY_NOT_FOUND": "modify.elementNotFound",
         "PREVIEW_VARIATION_ACK": "variation.ack"
      },
      'UPLOAD_STATES': {
         'NOTHING': '',
         'UPLOADING': 'Uploading',
         'PROCESSING': 'Processing',
         'COMPLETE': 'Complete'
      },
      'PasswordStatus': {
         NOT_VALIDATED: -1,
         VALID: 0,
         INVALID: 1
      }
   }

   return _constants;
});
