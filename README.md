Xsighten Cents
===========

### Setting up

Install [Redis](http://jasdeep.ca/2012/05/installing-redis-on-mac-os-x/), [Node.js and NPM] and [Grunt](http://gruntjs.com/).

`sudo npm install`

### Running

`grunt` to run the UI server, and automatically open the homepage.

`grunt test` to run Unit Tests and End to End Tests. (may require setup, check `/docs`)

### Give me more.

Find more documentation in `/docs`. May the force be with you.
