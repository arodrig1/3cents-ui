/*
*
* Test Suite for A/B Testing
*
*/

describe('/optimization', function() {

    // steps before each test case.
    // basically, navigates to A/B Testing home

    beforeEach(function () {

        browser.ignoreSynchronization = true;

        // open the mobile UI

        browser.get('http://localhost:12002/#/tags');

        // check for the side navigation bar

        browser.sleep(2000);

        element(by.css('.navigation > nav.ng-hide')).isPresent()
            .then(function(value) {

                //  open the side navigation bar

                if (value) return element(by.css('.navigation > button')).click();

            }).then(function(value) {

                // check for optimization links

                return element(by.css('.sub.ng-hide[href="#/abtesting"]')).isPresent()

            }).then(function(value) {

                // open the optimization links

                if (value) return element(by.css('.navbar > ul > li:nth-of-type(2) > div')).click();

            }).then(function() {

                // click on A/B Testing

                return element(by.css('.sub[href="#/abtesting"]')).click();

            }).then(function() {

                browser.sleep(1000); // sleep if a test is returning element still not available error
            });

    });

    // ------------------------------------------------------------

    describe('/apps', function() {

        it('should have the correct title', function() {

            expect(browser.getTitle()).toEqual('A/B Testing - Ensighten Mobile');

        });

        it('should list apps or show empty message', function() {

            element.all(by.css('.ab-app')).count()
                .then(function(value) {

                    if (value < 1) expect(element(by.css('.ab-app-empty > span:first-of-type')).getText()).toEqual('No Apps Found.');
                })
        });

        it('should show the form for creating a new app', function() {

            expect(element(by.css('.ab-modal.ab-modal-newapp')).isPresent()).toEqual(false);

            element(by.css('.ab-btn-newtest')).click()
                .then(function() {

                browser.sleep(1000); // waiting for $modal open

                expect(element(by.css('.ab-modal.ab-modal-newapp')).isPresent()).toEqual(true);
                expect(element(by.css('.ab-modal.ab-modal-newapp > h3')).getText()).toEqual('Create a new App');

                element(by.css('.modal-cancel')).click()
                    .then(function() {

                        browser.sleep(1000); // waiting for $modal.close
                    });
            })
        });

        it('should not allow save until all fields are filled', function() {

            element(by.css('.ab-btn-newtest')).click()
                .then(function() {

                    browser.sleep(1000); // waiting for $modal.open

                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    // fill all fields

                    element(by.model('newAppName')).sendKeys('E2E(A)-name');
                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    element(by.model('newAppId')).sendKeys('E2E(A)-id');
                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    element(by.model('newAppVersion')).sendKeys('E2E(A)-version');
                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    element.all(by.model('newAppPlatform')).get(1).click();
                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    element(by.model('newAppSpace')).sendKeys('prod');
                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual('true');

                    element(by.model('newAppDescription')).sendKeys('E2E(A)-description');

                    expect(element(by.css('.btn-app-create')).getAttribute('disabled')).toEqual(null);

                    element(by.css('.modal-cancel')).click()
                        .then(function() {

                            browser.sleep(1000); // waiting for $modal.close
                        });

            })
        });

        it('should update the list with a new app', function() {

            element(by.css('.ab-btn-newtest')).click()
                .then(function() {

                    browser.sleep(1000); // waiting for $modal.open

                    var app_count = 0;

                    element.all(by.css('.ab-app')).count()
                        .then(function(value) {

                            app_count = value;
                        });

                    var time_in_epoch = (new Date()).getTime();
                    var app_name = 'E2E(A)-name-' + time_in_epoch;
                    var app_version = 'E2E(A)-version-' + time_in_epoch;

                    element(by.model('newAppName')).sendKeys(app_name);
                    element(by.model('newAppId')).sendKeys('E2E(A)-id');
                    element(by.model('newAppVersion')).sendKeys(app_version);
                    element.all(by.model('newAppPlatform')).get(1).click();
                    element(by.model('newAppSpace')).sendKeys('Local-Saravanan');
                    element(by.model('newAppDescription')).sendKeys('E2E(A)-description');

                    // FIX API CALL to ENABLE THIS TEST

                    // requires changes in the API to go through, and then check for app_count + 1

                    // element(by.css('.btn-app-create')).click()
                    //     .then(function() {

                    //         browser.sleep(3000); // waiting for $modal.close
                    //         expect(element.all(by.css('.ab-app')).count()).toEqual(app_count + 1);
                    //     });
    
                    element(by.css('.modal-cancel')).click()
                        .then(function() {

                            browser.sleep(1000); // waiting for $modal.close
                        });
                });
            });
    });

    // ------------------------------------------------------------

    describe(':tests', function() {

        beforeEach(function() {

            // choose the Wordpress - Local app 

            element(by.css('.op-app-choose[data-app="Wordpress - Local"]')).click();
        });

        it('should list test or show empty message', function() {

            browser.sleep(3000);
            element.all(by.css('.ab-tests-new .ab-test-item')).count()
                .then(function(value) {

                    if (value < 1) {

                        expect(element(by.css('.ab-tests-new .ab-test-item-empty')).isPresent()).toEqual(true);
                        expect(element(by.css('.ab-tests-new .ab-test-item-empty')).getText()).toEqual('No Tests Found.');
                    }
                });

            element.all(by.css('.ab-tests-live .ab-test-item')).count()
                .then(function(value) {

                    if (value < 1) {

                        expect(element(by.css('.ab-tests-live .ab-test-item-empty')).isPresent()).toEqual(true);
                        expect(element(by.css('.ab-tests-live .ab-test-item-empty')).getText()).toEqual('No Tests Found.');

                    }
                });

            // find "Choose" button // this is done in beforeEach()

            // element.all(by.css(".op-app-choose")).count()
            //     .then(function (value) {

            //         if (value < 1) return;  // no apps 

            //         // if an app exists, click the first one

            //         element.all(by.css(".btn-cool.ab-button")).get(1).click()
            //             .then(function () {

                           
            //             })
            //      }
            //     });

            // Check for number of tests // this checks : if the empty items are present do they have the right text.
            // the goal is to check, if there no test-items, is test empty message present. 

            // element.all(by.css('.ab-test.ab-test-empty')).count()
            //     .then (function (value) {

            //         // If no tests, both values should say, "No Tests Found."
            //         if ( value < 3 ) { // this will always be 3
            //             expect(
            //                 element.all(by.css('.ab-test.ab-test-empty')).get(0).getText()
            //             ).toEqual("No Tests Found.");
            //             expect(
            //                 element.all(by.css('.ab-test.ab-test-empty')).get(1).getText()
            //             ).toEqual("No Tests Found.");

            //         // This should not exist if there are tests
            //         } else {
            //             expect (
            //                 element.all(by.css('.ab-test.ab-test-empty')).count()
            //             ).toBeLessThan(1);
            //         }
            //     });

        });

        it('should open a form for a new a/b test', function() {

            element.all(by.css('.ab-tests-new > .ab-tests-header a')).click()
                .then(function(){

                    browser.sleep(1000); // waiting for $modal.open

                    expect(element(by.css('.ab-modal.ab-modal-newtest')).isPresent()).toEqual(true);
                    expect(element(by.css('.ab-modal.ab-modal-newtest > h3')).getText()).toEqual('Create a new A/B Test');

                    element(by.css('.modal-cancel')).click()
                        .then(function() {

                            browser.sleep(1000); // waiting for $modal.close
                        });
                });
        });


        // it('should not allow save until fields are filled out', function() {

        // });

        // it('should open a form for a new a/b test', function() {

        // });

        // it('should not allow save until fields are filled out', function() {

        // });

        // it('should create a test, and redirect to it', function() {

        // });

    })

    // ------------------------------------------------------------

    describe(':variations', function() {

        beforeEach(function() {

        });

        // it('should show the choose screen button', function() {

        // });

        // it('should show options on selecting choose screen', function() {

        // });

        // it('should add two variations on choosing a screen', function() {

        // });

        // it('should show the edit and delete options on all variations', function() {

        // });

        // it('should add one more variation on clicking add new variation', function() {

        // });

        // it('should delete the variation on clicking delete', function() {

        // });

    });

    // ------------------------------------------------------------

    describe(':changes', function() {

        beforeEach(function() {

        });

        // it('should show only one variation after clicking edit', function() {

        // });

        // it('should show properties on element click', function() {

        // });

        // it('should show a textbox on editing a text property', function() {

        // });

        // it('should show the color picker on clicking a color property', function() {

        // });

    });

    // ------------------------------------------------------------

    describe(':history', function() {

        beforeEach(function() {

        });

        // it('should show the choose screen button', function() {

        // });

        // it('should show options on selecting choose screen', function() {

        // });

        // it('should add two variations on choosing a screen', function() {

        // });

    });

});
