var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function(path) {
  return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    allTestFiles.push(pathToModule(file));
  }
});

require.config({
  
  baseUrl: '/base', // karma serves all files from /base 

  paths : {

    // @ : use framework.library for name of module. used while importing inside 'define'
    // @ : the angular module name is different, it cannot be changed, it is predefined in the code

    'angular': 'public/scripts/lib/angular/angular', 
    'angular.mocks': 'public/scripts/lib/angular/angular-mocks',
    'angular.busy': 'public/components/angular-busy/dist/angular-busy',
    'angular.spectrum': 'public/components/angular-spectrum-colorpicker/dist/angular-spectrum-colorpicker',

    'angular.ace': 'public/scripts/lib/ui-ace.min',

    'angucomplete': 'public/scripts/lib/angucomplete',
    'angularAnimate': 'public/scripts/lib/angular/angular-animate.min',
    'angularBootstrap': 'public/scripts/lib/ui-bootstrap-tpls-0.12.0.min',
    'angularCookies': 'public/scripts/lib/angular/angular-cookies',
    'angularFileUpload': 'public/scripts/lib/angular-file-upload.min',
    'angularFileUploadShim': 'public/scripts/lib/angular-file-upload-shim.min',
    'angularRoute': 'public/scripts/lib/angular/angular-route.min',
    'angularTimer': 'public/scripts/lib/angular-timer.min',
    'ngDragDrop': 'public/components/angular-dragdrop/src/angular-dragdrop.min',

    'jquery': 'public/components/jquery/dist/jquery',
    'jquery-ui': 'public/components/jquery-ui/jquery-ui.min',
    
    'spectrum': 'public/components/spectrum/spectrum',
    
    'ace': 'public/scripts/lib/ace/ace',
    'reconnectingWebsocket': 'public/scripts/lib/reconnecting-websocket',
    'swfobject': 'public/scripts/lib/swfobject',
    'c3': 'public/scripts/lib/c3.min',
    'd3': 'public/scripts/lib/d3.min',
    'CommentStripper': 'public/scripts/lib/CommentStripper',

    'app': 'public/scripts/app', // defining a directory 
  },

  shim: {

      'angular': {

          deps: ['jquery'],
          exports: 'angular' // TODO investigate what angular exports by default
      },

      'angularAnimate': ['angular'],
      'angularCookies': ['angular'],
      'angularRoute': ['angular'],
      'angularBootstrap': ['angular'],
      'angularSocket': ['angular'],
      'angular.ace': ['angular'],
      'angularTimer': ['angular'],
      'angular.busy': ['angular', 'angularAnimate'],
      'angular.mocks': ['angular'],
      'augcomplete': ['angular'],
      'angular.spectrum': ['angular', 'spectrum'],
      'angularFileUpload': ['angularFileUploadShim','angular'],
      'ngDragDrop' : ['angular','jquery', 'jquery-ui'],

      'jquery-ui' : ['jquery'],
      
      'spectrum': ['jquery'],
      'c3': ['d3'],
      'reconnectingWebsocket': {
          exports: 'ReconnectingWebSocket' // TODO leave the name as is 
      },
  },

  // dynamically load all test files
  deps: allTestFiles, 

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
