define([

	'angular',
	'angular.mocks',
	'app/main'

], function() {

	describe('ABAppsCtrl', function() {

		beforeEach(angular.mock.module('mobileUI'));

		it('should be defined', angular.mock.inject(function($rootScope, $controller) {

			var $scope = $rootScope.$new();
			var ctrl = $controller('ABAppsCtrl', { $scope : $scope });

			expect($scope.message).to.equal('hello world');
		}));
	});
});